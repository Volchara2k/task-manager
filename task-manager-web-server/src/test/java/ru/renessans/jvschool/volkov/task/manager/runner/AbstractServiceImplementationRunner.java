package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.service.ProjectServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.TaskServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.UserServiceTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(ServiceImplementation.class)
@Suite.SuiteClasses(
        {
                ProjectServiceTest.class,
                TaskServiceTest.class,
                UserServiceTest.class
        }
)
public abstract class AbstractServiceImplementationRunner {
}