package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.renessans.jvschool.volkov.task.manager.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.ControllerImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Category({PositiveImplementation.class, ControllerImplementation.class})
public class ProjectControllerTest extends AbstractWebApplicationTest {

    @Test
    @SneakyThrows
    public void projectsTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/projects"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-list"));
    }

    @Test
    @SneakyThrows
    public void viewTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/view/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-view"));
    }

    @Test
    @SneakyThrows
    public void createGetTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-settable"));
    }

    @Test
    @SneakyThrows
    public void createPostTest() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle("title");
        projectDTO.setDescription("description");
        projectDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/project/create")
                .flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    @SneakyThrows
    public void removeTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/delete/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    @SneakyThrows
    public void editGetTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/edit/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-settable"));
    }

    @Test
    @SneakyThrows
    public void editPostTest() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle("title");
        projectDTO.setDescription("description");
        projectDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/project/edit/{id}", PROJECT.getId())
                .flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/project/view/{id}"));
    }

}