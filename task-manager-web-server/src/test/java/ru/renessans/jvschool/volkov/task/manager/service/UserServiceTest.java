package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.WebServer;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

import java.util.UUID;

@Setter
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebServer.class)
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @Test
    @SneakyThrows
    public void testAddUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString());
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
    }

    @Test
    @SneakyThrows
    public void testAddUserWithFirstName() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), firstName);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(firstName, addUserResponse.getFirstName());
    }

    @Test
    @SneakyThrows
    public void testAddUserWithRole() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final UserRoleType roleType = UserRoleType.USER;
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), roleType);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(roleType, addUserResponse.getUserRoleType());
    }

}