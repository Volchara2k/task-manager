package ru.renessans.jvschool.volkov.task.manager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.renessans.jvschool.volkov.task.manager.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import java.util.Date;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Category({PositiveImplementation.class, EndpointImplementation.class})
public class TaskRestEndpointTest extends AbstractWebApplicationTest {

    @NotNull
    private final String baseUrl = "/api";

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setProjectId(PROJECT.getId());
        taskDTO.setId(UUID.randomUUID().toString());
        taskDTO.setUserId(USER.getId());
        taskDTO.setStatus(UserOwnerStatus.NOT_STARTED);
        TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        timeFrameDTO.setCreationDate(new Date());
        taskDTO.setTimeFrame(timeFrameDTO);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(taskDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/task/create")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(TASK.getId());
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setUserId(USER.getId());

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(taskDTO);

        assert TASK.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/task/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(TASK.getId()))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    @SneakyThrows
    public void getByIdTest() {
        assert TASK.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/task/view/{id}", TASK.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(TASK.getId()));
    }

    @Test
    @SneakyThrows
    public void getAllTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/tasks"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/task/delete/{id}", TASK.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

}