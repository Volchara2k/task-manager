package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.renessans.jvschool.volkov.task.manager.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.ControllerImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Category({PositiveImplementation.class, ControllerImplementation.class})
public class TaskControllerTest extends AbstractWebApplicationTest {

    @Test
    @SneakyThrows
    public void tasksTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/tasks"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-list"));
    }

    @Test
    @SneakyThrows
    public void viewTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/view/{id}", TASK.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-view"));
    }

    @Test
    @SneakyThrows
    public void createGetTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-settable"));
    }

    @Test
    @SneakyThrows
    public void createPostTest() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle("title");
        taskDTO.setDescription("description");
        taskDTO.setProjectId(PROJECT.getId());
        taskDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/task/create")
                .flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    @SneakyThrows
    public void removeTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/delete/{id}", TASK.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    @SneakyThrows
    public void editGetTest() {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/task/edit/{id}", TASK.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("task/task-settable"));
    }

    @Test
    @SneakyThrows
    public void editPostTest() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle("title");
        taskDTO.setDescription("description");
        taskDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/task/edit/{id}", TASK.getId())
                .flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/task/view/{id}"));
    }

}