package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAddresseeException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidContentException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidSendEmailException;

import java.util.Collection;

public interface INotificatorService<T, V> {

    boolean notifySubscribers(
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException;

    boolean notifySubscribers(
            @Nullable T address,
            @Nullable V content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException;

    boolean notifySubscribers(
            @Nullable Collection<T> addressees,
            @Nullable V content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException;

}