package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.annotation.Admin;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IAdminSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterTaskUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.io.File;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@WebService
@Controller
@RequiredArgsConstructor
public class AdminSoapEndpoint implements IAdminSoapEndpoint {

    @NotNull
    private final IAuthenticationService authenticationService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IUserTaskService taskUserService;

    @NotNull
    private final IUserProjectService projectUserService;

    @NotNull
    private final IConfigurationService configurationService;

    @Admin
    @WebMethod
    @WebResult(name = "serverData", partName = "serverData")
    @Override
    public String serverData() {
        return StringUtils.join(
                this.configurationService.getServerHost(),
                File.separator,
                this.configurationService.getServerPort()
        );
    }

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @Override
    public UserLimitedDTO signUpUserWithUserRole(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "userRole", partName = "userRole") @Nullable final UserRoleType userRoleType
    ) throws InvalidParamException {
        @NotNull final User user = this.authenticationService.signUp(login, password, userRoleType);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteUserById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        return this.userService.deleteUserById(id);
    }

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws InvalidParamException {
        return this.userService.deleteUserByLogin(login);
    }

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteAllUsers() {
        return this.userService.deleteAllRecords();
    }

    @Admin
    @WebMethod
    @WebResult(name = "lockdownUserFlag", partName = "lockdownUserFlag")
    @NotNull
    @Override
    public UserLimitedDTO lockUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final User user = this.userService.lockUserByLogin(login);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "unLockdownUserFlag", partName = "unLockdownUserFlag")
    @NotNull
    @Override
    public UserLimitedDTO unlockUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final User user = this.userService.unlockUserByLogin(login);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "users", partName = "users")
    @Nullable
    @Override
    public Collection<UserLimitedDTO> setAllUsers(
            @WebParam(name = "users", partName = "users") @Nullable final Collection<UserLimitedDTO> usersDTO
    ) throws InvalidParamException {
        this.userService.importRecords(usersDTO != null ? usersDTO
                .stream()
                .map(userLimitedDTO -> AdapterUserUtil.forLimitedUser.apply(userLimitedDTO))
                .collect(Collectors.toList()) : null);
        return usersDTO;
    }

    @Admin
    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    @Override
    public Collection<UserLimitedDTO> getAllUsers() {
        return this.userService.exportRecords()
                .stream()
                .map(user -> AdapterUserUtil.forLimitedUserDTO.apply(user))
                .collect(Collectors.toList());
    }

    @Admin
    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllUsersTasks() {
        return this.taskUserService.exportRecords()
                .stream()
                .map(task -> AdapterTaskUtil.forTaskDTO.apply(task))
                .collect(Collectors.toList());
    }

    @Admin
    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @Nullable
    @Override
    public Collection<TaskDTO> setAllUsersTasks(
            @WebParam(name = "tasks", partName = "tasks") @Nullable final Collection<TaskDTO> tasksDTO
    ) throws InvalidParamException {
        this.taskUserService.importRecords(tasksDTO != null ? tasksDTO
                .stream()
                .map(taskDTO -> AdapterTaskUtil.forTask.apply(taskDTO))
                .collect(Collectors.toList()) : null);
        return tasksDTO;
    }

    @Admin
    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllUsersProjects() {
        return this.projectUserService.exportRecords()
                .stream()
                .map(project -> AdapterProjectUtil.forProjectDTO.apply(project))
                .collect(Collectors.toList());
    }

    @Admin
    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @Nullable
    @Override
    public Collection<ProjectDTO> setAllUsersProjects(
            @WebParam(name = "projects", partName = "projects") @Nullable final Collection<ProjectDTO> projectsDTO
    ) throws InvalidParamException {
        this.projectUserService.importRecords(projectsDTO != null ? projectsDTO
                .stream()
                .map(projectDTO -> AdapterProjectUtil.forProject.apply(projectDTO))
                .collect(Collectors.toList()) : null);
        return projectsDTO;
    }

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        @Nullable final User user = this.userService.getUserById(id);
        if (Objects.isNull(user)) return null;
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws InvalidParamException {
        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return null;
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    @Override
    public UserLimitedDTO editProfileById(
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final User user = this.userService.editUserProfileById(id, firstName);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    @Override
    public UserLimitedDTO editProfileByIdWithLastName(
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final User user = this.userService.editUserProfileById(id, firstName, lastName);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @Admin
    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @NotNull
    @Override
    public UserLimitedDTO updatePasswordById(
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "updatablePassword", partName = "updatablePassword") @Nullable final String updatablePassword
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final User user = this.userService.updateUserPasswordById(id, updatablePassword);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

}