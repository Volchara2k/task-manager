package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class UserRestEndpoint {

    @NotNull
    private final IUserService userService;

//    @NotNull
//    @GetMapping("/users")
//    @ApiOperation(
//            value = "Get all users",
//            notes = "Returns a complete list of users details."
//    )
//    public ResponseEntity<Collection<UserUnlimitedDTO>> getAllUsers() {
//        return ResponseEntity.ok(
//                this.userService.exportRecords()
//                        .stream()
//                        .map(user -> UserConverterUtil.forUnlimitedUserDTO.apply(user))
//                        .collect(Collectors.toList())
//        );
//    }

    @Nullable
    @SneakyThrows
    @GetMapping("/user/view/")
    @ApiOperation(
            value = "Get current user",
            notes = "Return current user details."
    )
    public ResponseEntity<UserLimitedDTO> getUserById() {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final User user = this.userService.getUserById(userid);
        if (Objects.isNull(user)) return null;
        return ResponseEntity.ok(
                AdapterUserUtil.forLimitedUserDTO.apply(user)
        );
    }

    @DeleteMapping("/user/delete/")
    @ApiOperation(
            value = "Delete current user",
            notes = "Returns integer deleted flag: 1 - true, 0 - false."
    )
    public ResponseEntity<Integer> deleteUserById() throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return ResponseEntity.ok(
                this.userService.deleteUserById(userid)
        );
    }

//    @Nullable
//    @SneakyThrows
//    @RequestMapping(
//            value = "/user/edit",
//            method = {RequestMethod.POST, RequestMethod.PUT}
//    )
//    @ApiOperation(
//            value = "Edit user",
//            notes = "Returns edited user. Edited user required."
//    )
//    public ResponseEntity<ProjectDTO> editUser(
//            @ApiParam(
//                    name = "projectDTO",
//                    type = "ProjectDTO",
//                    value = "Edited user",
//                    required = true
//            )
//            @RequestBody @NotNull final ProjectDTO projectDTO
//    ) {
//        @NotNull final Project project = ProjectConverterUtil.forProject.apply(projectDTO);
//        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
//        @NotNull final String userid = CurrentUserUtil.getUserId();
//        if (ValidRuleUtil.isNullOrEmpty(project.getUserId())) project.setUserId(userid);
//
//        try {
//            this.userService.addUserOwner(project);
//        } catch (@NotNull final Exception exception) {
//            return null;
//        }
//        return ResponseEntity.ok(projectDTO);
//    }

}