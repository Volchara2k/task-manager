package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEmailNotificatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEmailSenderService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAddresseeException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidContentException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidSendEmailException;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractEmailNotificatorService implements IEmailNotificatorService {

    @NotNull
    private final IEmailSenderService emailSenderService;

    @Override
    public boolean notifySubscribers(
            @Nullable final String address,
            @Nullable final String content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException {
        if (ValidRuleUtil.isNotNullOrEmpty(address)) {
            log.error("notify address error {}", InvalidAddresseeException.class);
            throw new InvalidAddresseeException();
        }
        if (ValidRuleUtil.isNotNullOrEmpty(content)) {
            log.error("notify address error {}", InvalidContentException.class);
            throw new InvalidContentException();
        }

        return this.notifySubscribers(
                Collections.singleton(address),
                content
        );
    }

    @Override
    public boolean notifySubscribers(
            @Nullable final Collection<String> addressees,
            @Nullable final String content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException {
        log.info("notify subscribers pending {}", addressees);
        if (Objects.isNull(addressees)) {
            log.error("notify subscribers error {}", InvalidAddresseeException.class);
            throw new InvalidAddresseeException();
        }
        if (ValidRuleUtil.isNullOrEmpty(content)) {
            log.error("notify subscribers error {}", InvalidContentException.class);
            throw new InvalidContentException();
        }

        @NotNull final String[] destinationsEmailAsArray = addressees.toArray(new String[0]);
        log.info("notify subscribers process {} elements", destinationsEmailAsArray.length);
        return this.notifyContentFor(destinationsEmailAsArray, content);
    }

    private boolean notifyContentFor(
            @NotNull final String[] emails, @NotNull final String content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException {
        log.info("notify process for {} elements", emails.length);
        final boolean isFaultSending = !this.emailSenderService.sendTo(emails, content);
        if (isFaultSending) return false;
        log.info("notify process for {} elements fine", emails.length);
        return true;
    }

}