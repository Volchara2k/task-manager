package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;

@Service
public class ConfigurationService implements IConfigurationService {

    @NotNull
    private final String host;

    @NotNull
    private final String port;

    @NotNull
    private final String pathnameBin;

    @NotNull
    private final String pathnameBase64;

    @NotNull
    private final String pathnameJson;

    @NotNull
    private final String pathnameXml;

    @NotNull
    private final String pathnameYaml;

    public ConfigurationService(
            @Value("{server.host}") @NotNull final String host,
            @Value("${server.port}") @NotNull final String port,
            @Value("${file.name.bin}") @NotNull final String pathnameBin,
            @Value("${file.name.base64}") @NotNull final String pathnameBase64,
            @Value("${file.name.json}") @NotNull final String pathnameJson,
            @Value("${file.name.xml}") @NotNull final String pathnameXml,
            @Value("${file.name.yaml}") @NotNull final String pathnameYaml
    ) {
        this.host = host;
        this.port = port;
        this.pathnameBin = pathnameBin;
        this.pathnameBase64 = pathnameBase64;
        this.pathnameJson = pathnameJson;
        this.pathnameXml = pathnameXml;
        this.pathnameYaml = pathnameYaml;
    }

    @NotNull
    @Override
    public String getServerHost() {
        return this.host;
    }

    @NotNull
    @Override
    public String getServerPort() {
        return this.port;
    }

    @NotNull
    @Override
    public String getBinPathname() {
        return this.pathnameBin;
    }

    @NotNull
    @Override
    public String getBase64Pathname() {
        return this.pathnameBase64;
    }

    @NotNull
    @Override
    public String getJsonPathname() {
        return this.pathnameJson;
    }

    @NotNull
    @Override
    public String getXmlPathname() {
        return this.pathnameXml;
    }

    @NotNull
    @Override
    public String getYamlPathname() {
        return this.pathnameYaml;
    }

}