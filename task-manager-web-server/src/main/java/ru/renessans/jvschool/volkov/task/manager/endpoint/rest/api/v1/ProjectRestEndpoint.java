package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest.IProjectRestEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    private final IUserProjectService userProjectService;

    @NotNull
    @GetMapping("/projects")
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of project details by order of creation."
    )
    @Override
    public ResponseEntity<Collection<ProjectDTO>> getAllProjects(
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return ResponseEntity.ok(
                this.userProjectService.getUserOwnerAll(userid)
                        .stream()
                        .map(project -> AdapterProjectUtil.forProjectDTO.apply(project))
                        .collect(Collectors.toList())
        );
    }

    @Nullable
    @GetMapping("/project/view/{id}")
    @ApiOperation(
            value = "Get project by ID",
            notes = "Returns project by unique ID. Unique ID required."
    )
    @Override
    public ResponseEntity<ProjectDTO> getProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userid, id);
        if (Objects.isNull(project)) return null;
        return ResponseEntity.ok(
                AdapterProjectUtil.forProjectDTO.apply(project)
        );
    }

    @Nullable
    @RequestMapping(
            value = "/project/create",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Create project",
            notes = "Returns created project. Created project required."
    )
    @Override
    public ResponseEntity<ProjectDTO> createProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Project project = AdapterProjectUtil.forProject.apply(projectDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        project.setUserId(userid);
        this.userProjectService.addUserOwner(project);
        return ResponseEntity.ok(
                projectDTO
        );
    }

    @DeleteMapping("/project/delete/{id}")
    @ApiOperation(
            value = "Delete project by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    @Override
    public ResponseEntity<Integer> deleteProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return ResponseEntity.ok(
                this.userProjectService.deleteUserOwnerById(userid, id)
        );
    }

    @Nullable
    @RequestMapping(
            value = "/project/edit",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Edit project",
            notes = "Returns edited project. Edited project required."
    )
    @Override
    public ResponseEntity<ProjectDTO> editProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Project project = AdapterProjectUtil.forProject.apply(projectDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        if (ValidRuleUtil.isNullOrEmpty(project.getUserId())) project.setUserId(userid);
        this.userProjectService.addUserOwner(project);
        return ResponseEntity.ok(
                projectDTO
        );
    }

}