package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICsvReaderService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.util.ResourceUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.CvsUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.io.IOException;
import java.util.Collection;

public abstract class AbstractReaderService<T> implements ICsvReaderService<T> {

    @NotNull
    private final Class<T> parsableClass;

    protected AbstractReaderService(
            @NotNull final Class<T> tClass
    ) {
        this.parsableClass = tClass;
    }

    @NotNull
    @Override
    public Collection<T> readFromResources(
            @Nullable final String pathname
    ) throws InvalidParamException, IOException {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        return CvsUtil.parseFromFor(
                ResourceUtil.fileFrom(pathname),
                this.parsableClass
        );
    }

}