package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.annotation.Admin;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IAdminSoapEndpoint {

    @Admin
    @WebMethod
    @WebResult(name = "serverData", partName = "serverData")
    String serverData();

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    UserLimitedDTO signUpUserWithUserRole(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRoleType userRoleType
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteUserById(
            @Nullable String id
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteAllUsers();

    @Admin
    @WebMethod
    @WebResult(name = "lockdownUserFlag", partName = "lockdownUserFlag")
    @NotNull
    UserLimitedDTO lockUserByLogin(
            @Nullable String login
    ) throws InvalidParamException, NotFoundUserException;

    @Admin
    @WebMethod
    @WebResult(name = "unLockdownUserFlag", partName = "unLockdownUserFlag")
    @NotNull
    UserLimitedDTO unlockUserByLogin(
            @Nullable String login
    ) throws InvalidParamException, NotFoundUserException;

    @Admin
    @WebMethod
    @WebResult(name = "users", partName = "users")
    @Nullable
    Collection<UserLimitedDTO> setAllUsers(
            @Nullable Collection<UserLimitedDTO> usersDTO
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    Collection<UserLimitedDTO> getAllUsers();

    @Admin
    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    Collection<TaskDTO> getAllUsersTasks();

    @Admin
    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @Nullable
    Collection<TaskDTO> setAllUsersTasks(
            @Nullable Collection<TaskDTO> tasksDTO
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    Collection<ProjectDTO> getAllUsersProjects();

    @Admin
    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @Nullable
    Collection<ProjectDTO> setAllUsersProjects(
            @Nullable Collection<ProjectDTO> projectsDTO
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO getUserById(
            @Nullable String id
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO getUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    UserLimitedDTO editProfileById(
            @Nullable String id,
            @Nullable String firstName
    ) throws InvalidParamException, NotFoundUserException;

    @Admin
    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    UserLimitedDTO editProfileByIdWithLastName(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    ) throws InvalidParamException, NotFoundUserException;

    @Admin
    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @NotNull
    UserLimitedDTO updatePasswordById(
            @Nullable String id,
            @Nullable String updatablePassword
    ) throws InvalidParamException, NotFoundUserException;

}