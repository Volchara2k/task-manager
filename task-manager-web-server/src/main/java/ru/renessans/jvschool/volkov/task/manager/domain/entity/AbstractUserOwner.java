package ru.renessans.jvschool.volkov.task.manager.domain.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;

import javax.persistence.*;

@Getter
@Setter
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwner extends AbstractModel {

    @NotNull
    @Builder.Default
    @Column(
            nullable = false,
            length = 50
    )
    private String title = "";

    @NotNull
    @Builder.Default
    @Column(
            nullable = false
    )
    private String description = "";

    @Nullable
    @Column(
            length = 50
    )
    private String userId;

    @NotNull
    @Builder.Default
    private TimeFrame timeFrame = new TimeFrame();

    @NotNull
    @Builder.Default
    @Column(
            nullable = false,
            length = 25
    )
    @Enumerated(EnumType.STRING)
    private UserOwnerStatus status = UserOwnerStatus.NOT_STARTED;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mapping_user_id")
    private User user;

}