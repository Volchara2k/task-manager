package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum UserRoleType {

    USER("Пользователь"),

    ADMIN("Администратор"),

    SUPPORT_SPECIALIST("Специалист поддержки"),

    TECHNICAL_SPECIALIST("Технический специалист"),

    ARCHITECT("Архитектор");

    @NotNull
    private final String title;

    UserRoleType(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    public boolean isAdmin() {
        return this == ADMIN;
    }

}