package ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;

import java.util.Collection;

@SuppressWarnings("unused")
public interface IProjectRestEndpoint {

    @NotNull
    @GetMapping
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of project details by order of creation."
    )
    ResponseEntity<Collection<ProjectDTO>> getAllProjects() throws InvalidParamException;

    @Nullable
    @GetMapping("/project/view/{id}")
    @ApiOperation(
            value = "Get project by ID",
            notes = "Returns project by unique ID. Unique ID required."
    )
    ResponseEntity<ProjectDTO> getProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    ) throws InvalidParamException;

    @Nullable
    @RequestMapping(
            value = "/project/create",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Create project",
            notes = "Returns created project. Created project required."
    )
    ResponseEntity<ProjectDTO> createProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @DeleteMapping("/project/delete/{id}")
    @ApiOperation(
            value = "Delete project by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    ResponseEntity<Integer> deleteProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    ) throws InvalidParamException;

    @Nullable
    @RequestMapping(
            value = "/project/edit",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Edit project",
            notes = "Returns edited project. Edited project required."
    )
    ResponseEntity<ProjectDTO> editProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

}