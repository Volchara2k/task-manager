package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.ITaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterTaskUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@WebService
@RequiredArgsConstructor
public class TaskSoapEndpoint implements ITaskSoapEndpoint {

    @NotNull
    private final IUserTaskService taskUserService;

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @NotNull
    @Override
    public TaskDTO addTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull final TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Task request = AdapterTaskUtil.forTask.apply(taskDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        request.setUserId(userid);
        @NotNull final Task response = this.taskUserService.addUserOwner(request);
        return AdapterTaskUtil.forTaskDTO.apply(response);
    }

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @NotNull
    @Override
    public TaskDTO updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull final TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Task request = AdapterTaskUtil.forTask.apply(taskDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        if (ValidRuleUtil.isNullOrEmpty(request.getUserId())) request.setUserId(userid);
        @NotNull final Task response = this.taskUserService.addUserOwner(request);
        return AdapterTaskUtil.forTaskDTO.apply(response);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return this.taskUserService.deleteUserOwnerById(userid, id);
    }

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    @Override
    public TaskDTO getTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final Task task = taskUserService.getUserOwnerById(userid, id);
        if (Objects.isNull(task)) return null;
        return AdapterTaskUtil.forTaskDTO.apply(task);
    }

    @WebMethod
    @WebResult(name = "tasksDTO", partName = "tasksDTO")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllTasks(
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return this.taskUserService.getUserOwnerAll(userid)
                .stream()
                .map(task -> AdapterTaskUtil.forTaskDTO.apply(task))
                .collect(Collectors.toList());
    }

}