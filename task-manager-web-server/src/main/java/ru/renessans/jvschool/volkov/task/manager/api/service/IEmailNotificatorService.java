package ru.renessans.jvschool.volkov.task.manager.api.service;

public interface IEmailNotificatorService extends INotificatorService<String, String> {
}