package ru.renessans.jvschool.volkov.task.manager.api.service;

public interface IEmailSenderService extends ISenderService<String, String> {
}