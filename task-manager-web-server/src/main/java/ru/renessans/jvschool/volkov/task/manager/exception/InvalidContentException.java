package ru.renessans.jvschool.volkov.task.manager.exception;

public final class InvalidContentException extends AbstractException {

    public InvalidContentException() {
        super("Обнаружены недопустимый контент сообщения!");
    }

}