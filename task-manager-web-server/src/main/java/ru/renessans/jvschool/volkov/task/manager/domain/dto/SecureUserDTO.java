package ru.renessans.jvschool.volkov.task.manager.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

@Getter
@Setter
@ApiModel(description = "All details about the current secured user")
public final class SecureUserDTO extends User {

    @Nullable
    @ApiModelProperty(
            value = "Unique ID of user",
            example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
            name = "id",
            required = true
    )
    private String id;

    @Nullable
    @ApiModelProperty(
            value = "Unique email of user",
            example = "vvolkov@rencredit.ru",
            name = "email",
            required = true
    )
    private String email;

    @Nullable
    @ApiModelProperty(
            value = "First name of user",
            example = "Valery",
            name = "firstName",
            required = true
    )
    private String firstName;

    @Nullable
    @ApiModelProperty(
            value = "Last name of user",
            example = "Volkov",
            name = "lastName"
    )
    private String lastName;

    @Nullable
    @ApiModelProperty(
            value = "Role of user",
            name = "userRoleType"
    )
    private UserRoleType userRoleType;

    @Builder(
            builderMethodName = "detailBuilder"
    )
    public SecureUserDTO(
            @NotNull final UserDetails userDetails,
            @Nullable final String id,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final UserRoleType userRoleType
    ) {
        super(
                userDetails.getUsername(), userDetails.getPassword(),
                userDetails.isEnabled(), userDetails.isAccountNonExpired(),
                userDetails.isCredentialsNonExpired(), userDetails.isAccountNonLocked(),
                userDetails.getAuthorities()
        );
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRoleType = userRoleType;
    }

}