package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@SuppressWarnings("unused")
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry viewControllerRegistry) {
        viewControllerRegistry
                .addViewController("/fontawesome")
                .setViewName("/fontawesome");
    }

}