package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.AbstractModel;

import java.util.Collection;

@SuppressWarnings("unused")
public interface IDaoService<E extends AbstractModel> {

    @NotNull
    E save(@Nullable E value) throws InvalidParamException;

    @Nullable
    E getRecordById(@Nullable final String id) throws InvalidParamException;

    boolean existsRecordById(@Nullable final String id) throws InvalidParamException;

    int deleteRecordById(@NotNull String id) throws InvalidParamException;

    @Nullable
    E deleteRecord(@Nullable E value) throws InvalidParamException;

    int deleteAllRecords();

    @NotNull
    Collection<E> exportRecords();

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Collection<E> importRecords(@Nullable Collection<E> values) throws InvalidParamException;

}