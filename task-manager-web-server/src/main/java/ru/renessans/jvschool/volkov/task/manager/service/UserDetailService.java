package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserDetailService implements UserDetailsService {

    @NotNull
    private final IUserService userService;

    @Nullable
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(
            @Nullable final String login
    ) throws UsernameNotFoundException {
        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) throw new UsernameNotFoundException("Пользователь не найден!");
        return AdapterUserUtil.forUserDetail.apply(user);
    }

}