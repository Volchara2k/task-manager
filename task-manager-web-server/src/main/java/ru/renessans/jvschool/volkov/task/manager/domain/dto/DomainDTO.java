package ru.renessans.jvschool.volkov.task.manager.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Collection;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "All details about the domain")
public final class DomainDTO extends AbstractEntityDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "Projects",
            name = "projects",
            required = true
    )
    private Collection<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "Tasks",
            name = "tasks",
            required = true
    )
    private Collection<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "Users",
            name = "users",
            required = true
    )
    private Collection<UserUnlimitedDTO> users = new ArrayList<>();

}