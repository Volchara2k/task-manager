package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.annotation.Admin;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;

import java.util.Collection;

@Controller
@RequiredArgsConstructor
public class AdminController extends AbstractController {

    @NotNull
    private final IUserTaskService userTaskService;

    @NotNull
    private final IUserProjectService userProjectService;

    @Admin
    @NotNull
    @SneakyThrows
    @GetMapping("/tasks/dump")
    public ModelAndView tasksDump() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("dump-tasks");
        @NotNull final Collection<Task> tasks = this.userTaskService.exportRecords();
        modelAndView.addObject("tasks", tasks);
        return super.navBlockView.apply(modelAndView);
    }

    @Admin
    @NotNull
    @SneakyThrows
    @GetMapping("/projects/dump")
    public ModelAndView projectsDump() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("dump-projects");
        @NotNull final Collection<Project> projects = this.userProjectService.exportRecords();
        modelAndView.addObject("projects", projects);
        return super.navBlockView.apply(modelAndView);
    }

}