package ru.renessans.jvschool.volkov.task.manager.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@JacksonXmlRootElement(localName = "error")
public final class ApiErrorResponseDTO {

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime timestamp;

    @NotNull
    private HttpStatus status;

    @NotNull
    private String message;

    public ApiErrorResponseDTO(
            @NotNull final HttpStatus status,
            @NotNull final Exception exception
    ) {
        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.message = exception.getLocalizedMessage();
    }

}