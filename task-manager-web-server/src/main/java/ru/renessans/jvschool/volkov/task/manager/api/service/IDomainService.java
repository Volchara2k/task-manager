package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;

public interface IDomainService {

    @NotNull
    DomainDTO dataImport(@Nullable DomainDTO domain) throws InvalidParamException;

    @NotNull
    DomainDTO dataExport(@Nullable DomainDTO domain) throws InvalidParamException;

}