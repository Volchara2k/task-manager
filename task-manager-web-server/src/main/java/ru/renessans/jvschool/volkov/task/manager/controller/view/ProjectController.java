package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.annotation.Auth;
import ru.renessans.jvschool.volkov.task.manager.api.controller.IProjectController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;

import java.util.Collection;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
@SuppressWarnings("SpringMVCViewInspection")
public class ProjectController extends AbstractController implements IProjectController {

    @NotNull
    private final IUserProjectService userProjectService;

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/projects")
    @Override
    public ModelAndView list(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("projects");
        @NotNull final Collection<Project> projects = this.userProjectService.getUserOwnerAll(userDTO.getId());
        modelAndView.addObject("projects", projects);
        return super.navBlockView.apply(modelAndView);
    }

    @Auth
    @NotNull
    @GetMapping("/project/create")
    @Override
    public ModelAndView create(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project-create");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(userDTO.getId());
        modelAndView.addObject("project", projectDTO);
        modelAndView.addObject("statuses", UserOwnerStatus.values());
        return super.navAndOverlayBlocksView.apply(modelAndView);
    }

    @Auth
    @NotNull
    @PostMapping("/project/create")
    @SneakyThrows
    @Override
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @NotNull final Project project = AdapterProjectUtil.forProject.apply(projectDTO);
        this.userProjectService.addUserOwner(project);
        return new ModelAndView("redirect:/projects");
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @Override
    public ModelAndView view(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(project)) throw new NotFoundUserOwnerException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project-view");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/project/delete/{id}")
    @Override
    public ModelAndView delete(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        this.userProjectService.deleteUserOwnerById(userDTO.getId(), id);
        return new ModelAndView("redirect:/projects");
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(project)) throw new NotFoundUserOwnerException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project-updatable");
        @NotNull final ProjectDTO projectDTO = AdapterProjectUtil.forProjectDTO.apply(project);
        modelAndView.addObject("project", projectDTO);
        return modelAndView;
    }

    @Auth
    @NotNull
    @SneakyThrows
    @PostMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @NotNull final Project project = AdapterProjectUtil.forProject.apply(projectDTO);
        this.userProjectService.addUserOwner(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project/view/{id}");
        modelAndView.addObject("id", project.getId());
        return modelAndView;
    }

}