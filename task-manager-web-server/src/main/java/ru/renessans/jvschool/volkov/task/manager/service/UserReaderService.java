package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserReaderService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.pojo.UserPOJO;

import java.io.IOException;
import java.util.Collection;

@Slf4j
@Service
@SuppressWarnings("unused")
public class UserReaderService extends AbstractReaderService<UserPOJO> implements IUserReaderService {

    @NotNull
    private final String cvsPathname;

    public UserReaderService(
            @Value("${cvs.pathname.user}") @NotNull final String cvsPathname
    ) {
        super(UserPOJO.class);
        this.cvsPathname = cvsPathname;
    }

    @NotNull
    @Override
    public Collection<UserPOJO> readFromResources(
    ) throws IOException, InvalidParamException {
        return super.readFromResources(this.cvsPathname);
    }

}