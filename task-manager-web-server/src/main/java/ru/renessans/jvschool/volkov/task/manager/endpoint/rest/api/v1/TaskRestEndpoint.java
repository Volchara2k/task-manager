package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest.ITaskRestEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterTaskUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @NotNull
    private final IUserTaskService userTaskService;

    @NotNull
    @GetMapping("/tasks")
    @ApiOperation(
            value = "Get all tasks",
            notes = "Returns a complete list of tasks details by order of creation."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "No tasks found")
    })
    @Override
    public ResponseEntity<Collection<TaskDTO>> getAllTasks() throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return ResponseEntity.ok(
                this.userTaskService.getUserOwnerAll(userid)
                        .stream()
                        .map(task -> AdapterTaskUtil.forTaskDTO.apply(task))
                        .collect(Collectors.toList())
        );
    }

    @Nullable
    @GetMapping("/task/view/{id}")
    @ApiOperation(
            value = "Get task by ID",
            notes = "Returns task by unique ID. Unique ID required."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Invalid task ID"),
            @ApiResponse(code = 403, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @Override
    public ResponseEntity<TaskDTO> getTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final Task task = this.userTaskService.getUserOwnerById(userid, id);
        return ResponseEntity.ok(
                AdapterTaskUtil.forTaskDTO.apply(task)
        );
    }

    @Nullable
    @RequestMapping(
            value = "/task/create",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Create task",
            notes = "Returns created task. Created task required."
    )
    @Override
    public ResponseEntity<TaskDTO> createTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Created task",
                    required = true
            )
            @RequestBody @NotNull final TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Task task = AdapterTaskUtil.forTask.apply(taskDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        task.setUserId(userid);
        this.userTaskService.addUserOwner(task);
        return ResponseEntity.ok(
                taskDTO
        );
    }

    @NotNull
    @DeleteMapping("/task/delete/{id}")
    @ApiOperation(
            value = "Delete task by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    @Override
    public ResponseEntity<Integer> deleteTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return ResponseEntity.ok(
                this.userTaskService.deleteUserOwnerById(userid, id)
        );
    }

    @Nullable
    @RequestMapping(
            value = "/task/edit",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Edit task",
            notes = "Returns edited task. Edited task required."
    )
    @Override
    public ResponseEntity<TaskDTO> editTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Edited task",
                    required = true
            )
            @RequestBody @NotNull final TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Task task = AdapterTaskUtil.forTask.apply(taskDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        if (ValidRuleUtil.isNullOrEmpty(task.getUserId())) task.setUserId(userid);
        this.userTaskService.addUserOwner(task);
        return ResponseEntity.ok(
                taskDTO
        );
    }

}