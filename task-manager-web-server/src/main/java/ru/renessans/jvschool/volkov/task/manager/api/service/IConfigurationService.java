package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConfigurationService {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getBinPathname();

    @NotNull
    String getBase64Pathname();

    @NotNull
    String getJsonPathname();

    @NotNull
    String getXmlPathname();

    @NotNull
    String getYamlPathname();

}