package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import java.io.IOException;
import java.util.Collection;

@SuppressWarnings("unused")
public interface IUserService extends IDaoService<User> {

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password
    ) throws InvalidParamException;

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    ) throws InvalidParamException;

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRoleType userRoleTypes
    ) throws InvalidParamException;

    @Nullable
    User getUserById(
            @Nullable String id
    ) throws InvalidParamException;

    @Nullable
    User getUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    boolean existsUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    @Nullable
    UserRoleType getUserRole(
            @Nullable String userId
    ) throws InvalidParamException;

    @NotNull
    User editUserProfileById(
            @Nullable String id,
            @Nullable String firstName
    ) throws InvalidParamException, NotFoundUserException;

    @NotNull
    User editUserProfileById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    ) throws InvalidParamException, NotFoundUserException;

    @NotNull
    User updateUserPasswordById(
            @Nullable String id,
            @Nullable String updatablePassword
    ) throws InvalidParamException, NotFoundUserException;

    @NotNull
    User lockUserByLogin(
            @Nullable String login
    ) throws InvalidParamException, NotFoundUserException;

    @NotNull
    User unlockUserByLogin(
            @Nullable String login
    ) throws InvalidParamException, NotFoundUserException;

    int deleteUserById(
            @Nullable String id
    ) throws InvalidParamException;

    int deleteUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    @NotNull
    Collection<User> initialDemoUsers(
    ) throws InvalidParamException, IOException;

}