package ru.renessans.jvschool.volkov.task.manager.configuration;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

@Configuration
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @NotNull
    private final UserDetailsService userDetailsService;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    @Bean
    @NotNull
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(
            @NotNull final AuthenticationManagerBuilder authenticationManagerBuilder
    ) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(this.passwordEncoder);
    }

    @Override
    protected void configure(
            @NotNull final HttpSecurity httpSecurity
    ) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers("/", "/sign-in*", "/error").permitAll()
                .antMatchers("/css/**", "/js/**", "/webjars/**").permitAll()
                .antMatchers("/bootstrap/**", "/components/**", "/less/**", "/plugins/**").permitAll()
                .antMatchers("/ws/*").permitAll()
                .antMatchers("/swagger-ui.html")
                .hasAnyRole(
                        UserRoleType.ARCHITECT.name(), UserRoleType.ADMIN.name()
                )
                .antMatchers("/v2/api-docs").hasRole(UserRoleType.ARCHITECT.name())
                .antMatchers("/api/v1/**").permitAll()
                //.anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/sign-in")
                .loginProcessingUrl("/auth")
                .defaultSuccessUrl("/", true)
                .failureUrl("/sign-in-error")
                .and()
                .logout().permitAll()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .and()
                .csrf().disable();
    }

}