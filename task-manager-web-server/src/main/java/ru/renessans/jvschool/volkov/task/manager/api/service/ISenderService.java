package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAddresseeException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidContentException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidSendEmailException;

public interface ISenderService<T, V> {

    boolean sendTo(
            @Nullable T[] to, @Nullable V content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException;

}