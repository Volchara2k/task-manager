package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.annotation.Admin;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public interface IDataInterChangeSoapEndpoint {

    @Admin
    @WebMethod
    @WebResult(name = "clearBinDataFlag", partName = "clearBinDataFlag")
    boolean dataBinClear();

    @Admin
    @WebMethod
    @WebResult(name = "clearBase64DataFlag", partName = "clearBase64DataFlag")
    boolean dataBase64Clear();

    @Admin
    @WebMethod
    @WebResult(name = "clearJsonDataFlag", partName = "clearJsonDataFlag")
    boolean dataJsonClear();

    @Admin
    @WebMethod
    @WebResult(name = "clearXmlDataFlag", partName = "clearXmlDataFlag")
    boolean dataXmlClear();

    @Admin
    @WebMethod
    @WebResult(name = "clearYamlDataFlag", partName = "clearYamlDataFlag")
    boolean dataYamlClear();

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataBin() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataBase64() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataJson() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataXml() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataYaml() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataBin() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataBase64() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataJson() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataXml() throws InvalidParamException;

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataYaml() throws InvalidParamException;

}