package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.domain.pojo.UserPOJO;

public interface IUserReaderService extends ICsvReaderService<UserPOJO> {
}