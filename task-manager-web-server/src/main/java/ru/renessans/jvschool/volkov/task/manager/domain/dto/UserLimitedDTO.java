package ru.renessans.jvschool.volkov.task.manager.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLimitedDTO extends AbstractEntityDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Builder.Default
    @NotBlank(
            message = "Логин не должен быть пустым!"
    )
    @ApiModelProperty(
            value = "Unique login of user",
            example = "valery2000",
            name = "login",
            required = true
    )
    @Size(
            min = 4,
            max = 25,
            message = "Логин не должен быть менее 4 символов, и не более 25!"
    )
    private String login = "login";

    @NotNull
    @Builder.Default
    @NotBlank(
            message = "Электронная почта не должна быть пустой!"
    )
    @Size(
            min = 10,
            max = 30,
            message = "Электронная почта не должна быть менее 10 символов, и не более 30!"
    )
    @ApiModelProperty(
            value = "Unique email of user",
            example = "valery2000@yandex.ru",
            name = "email",
            required = true
    )
    @Email(
            regexp = "([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@gmail([.])ru",
            message = "Введите корректный адрес электронной почты!"
    )
    private String email = "email@gmail.com";

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "First name of user",
            example = "Valery",
            name = "firstName",
            required = true
    )
    @Size(
            min = 2,
            max = 25,
            message = "Имя не должно быть более менее 2 символов, и не более 25!"
    )
    private String firstName = "firstName";

    @Nullable
    @Builder.Default
    @ApiModelProperty(
            value = "Last name of user",
            example = "Volkov",
            name = "lastName"
    )
    @Size(
            min = 2,
            max = 25,
            message = "Фамилия не должна быть менее 2 символом, и не более 25!"
    )
    private String lastName = "";

    @Nullable
    @Builder.Default
    @ApiModelProperty(
            value = "MiddleName of user",
            example = "Sergeevich",
            name = "middleName"
    )
    @Size(
            min = 2,
            max = 25,
            message = "Отчество не должно быть менее 2 символов, и не более 25!"
    )
    private String middleName = "";

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(getLogin());
        result.append(", имя: ").append(getFirstName()).append("\n");
        if (StringUtils.hasText(getLastName()))
            result.append(", фамилия: ").append(getLastName()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}