package ru.renessans.jvschool.volkov.task.manager.api.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.annotation.Auth;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;

@SuppressWarnings("unused")
public interface IProjectController {

    @Auth
    @NotNull
    @GetMapping("/projects")
    ModelAndView list(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @Auth
    @NotNull
    @GetMapping("/project/create")
    ModelAndView create(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @Auth
    @NotNull
    @PostMapping("/project/create")
    ModelAndView create(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

    @Auth
    @NotNull
    @GetMapping("/project/view/{id}")
    ModelAndView view(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @GetMapping("/project/delete/{id}")
    ModelAndView delete(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @GetMapping("/project/edit/{id}")
    ModelAndView edit(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @PostMapping("/project/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

}