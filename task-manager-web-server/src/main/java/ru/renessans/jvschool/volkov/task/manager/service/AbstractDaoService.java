package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDaoService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.repository.IDaoRepository;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@Transactional(
        rollbackFor = Exception.class
)
@RequiredArgsConstructor
public abstract class AbstractDaoService<E extends AbstractModel> implements IDaoService<E> {

    @NotNull
    private final IDaoRepository<E> repository;

    @NotNull
    @Override
    public E save(@Nullable final E value) throws InvalidParamException {
        if (Objects.isNull(value)) throw new InvalidParamException("value");
        return this.repository.save(value);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public E getRecordById(@Nullable final String id) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.repository.getRecordById(id);
    }

    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public boolean existsRecordById(@Nullable final String id) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.repository.existsById(id);
    }

    @Override
    public int deleteRecordById(@Nullable final String id) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        try {
            this.repository.deleteById(id);
        } catch (@NotNull final Exception exception) {
            return 0;
        }
        return 1;
    }

    @Nullable
    @Override
    public E deleteRecord(@Nullable final E value) throws InvalidParamException {
        if (Objects.isNull(value)) throw new InvalidParamException("value");
        this.repository.delete(value);
        return value;
    }

    @Override
    public int deleteAllRecords() {
        try {
            this.repository.deleteAll();
        } catch (@NotNull final Exception exception) {
            return 0;
        }
        return 1;
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public Collection<E> exportRecords() {
        return this.repository.findAll();
    }

    @NotNull
    @Override
    public Collection<E> importRecords(@Nullable final Collection<E> values) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidParamException("values");
        return this.repository.saveAll(values);
    }

}