package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

public interface IAuthenticationService {

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password
    ) throws InvalidParamException;

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws InvalidParamException;

    @NotNull
    @SuppressWarnings("unused")
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRoleType userRoleType
    ) throws InvalidParamException;

}