package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.TimeFrame;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@UtilityClass
public class AdapterProjectUtil {

    @NotNull
    public Function<Project, ProjectDTO> forProjectDTO = project -> ProjectDTO.builder()
            .id(project.getId())
            .userId(project.getUserId())
            .title(project.getTitle())
            .description(project.getDescription())
            .timeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(project.getTimeFrame().getCreationDate())
                            .startDate(project.getTimeFrame().getStartDate())
                            .endDate(project.getTimeFrame().getEndDate())
                            .build()
            )
            .status(project.getStatus())
            .build();

    @NotNull
    public Function<ProjectDTO, Project> forProject = projectDTO -> {
        if (ValidRuleUtil.isNullOrEmpty(projectDTO.getId())) projectDTO.setId(UUID.randomUUID().toString());
        if (Objects.isNull(projectDTO.getTimeFrame()) || Objects.isNull(projectDTO.getTimeFrame().getCreationDate()))
            projectDTO.setTimeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(new Date())
                            .startDate(projectDTO.getTimeFrame().getStartDate())
                            .endDate(projectDTO.getTimeFrame().getEndDate())
                            .build()
            );
        if (Objects.isNull(projectDTO.getStatus())) projectDTO.setStatus(UserOwnerStatus.NOT_STARTED);

        return Project.builder()
                .id(projectDTO.getId())
                .userId(projectDTO.getUserId())
                .title(projectDTO.getTitle())
                .description(projectDTO.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(Objects.requireNonNull(projectDTO.getTimeFrame().getCreationDate()))
                                .startDate(projectDTO.getTimeFrame().getStartDate())
                                .endDate(projectDTO.getTimeFrame().getEndDate())
                                .build()
                )
                .status(projectDTO.getStatus())
                .build();
    };

}