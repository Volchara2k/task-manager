package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class NotFoundUserException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER = "Ожидаемый пользователь не найден!";

    public NotFoundUserException() {
        super(EMPTY_USER);
    }

}