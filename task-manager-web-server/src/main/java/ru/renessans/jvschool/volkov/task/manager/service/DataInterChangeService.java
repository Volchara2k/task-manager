package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.utility.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.DataSerializerUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.FileUtil;

@Service
@RequiredArgsConstructor
public class DataInterChangeService implements IDataInterChangeService {

    @NotNull
    private final IDomainService domainService;

    @NotNull
    private final IConfigurationService configurationService;

    @Override
    public boolean dataBinClear() {
        @NotNull final String pathname = this.configurationService.getBinPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataBase64Clear() {
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataJsonClear() {
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataXmlClear() {
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataYamlClear() {
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        return FileUtil.delete(pathname);
    }

    @NotNull
    @Override
    public DomainDTO exportDataBin() throws InvalidParamException {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        @NotNull final DomainDTO dataExport = this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getBinPathname();
        return DataSerializerUtil.writeToBin(dataExport, pathname);
    }

    @NotNull
    @Override
    public DomainDTO exportDataBase64() throws InvalidParamException {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        @NotNull final DomainDTO dataExport = this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        return DataSerializerUtil.writeToBase64(dataExport, pathname);
    }

    @NotNull
    @Override
    public DomainDTO exportDataJson() throws InvalidParamException {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        @NotNull final DomainDTO dataExport = this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        return DataMarshalizerUtil.writeToJson(dataExport, pathname);
    }

    @NotNull
    @Override
    public DomainDTO exportDataXml() throws InvalidParamException {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        @NotNull final DomainDTO dataExport = this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        return DataMarshalizerUtil.writeToXml(dataExport, pathname);
    }

    @NotNull
    @Override
    public DomainDTO exportDataYaml() throws InvalidParamException {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        @NotNull final DomainDTO dataExport = this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        return DataMarshalizerUtil.writeToYaml(dataExport, pathname);
    }

    @NotNull
    @Override
    public DomainDTO importDataBin() throws InvalidParamException {
        @NotNull final String pathname = this.configurationService.getBinPathname();
        @NotNull final DomainDTO domainDTO = DataSerializerUtil.readFromBin(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataBase64() throws InvalidParamException {
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        @NotNull final DomainDTO domainDTO = DataSerializerUtil.readFromBase64(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataJson() throws InvalidParamException {
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromJson(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataXml() throws InvalidParamException {
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromXml(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataYaml() throws InvalidParamException {
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromYaml(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

}