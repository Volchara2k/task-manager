package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

import java.util.function.UnaryOperator;

public abstract class AbstractController {

    @NotNull
    protected final UnaryOperator<ModelAndView> navBlockView = modelAndView -> {
        try {
            modelAndView.addObject("userName", CurrentUserUtil.firstName.get());
        } catch (@NotNull final AccessDeniedException exception) {
            modelAndView.addObject("userName", "");
        }
        return modelAndView;
    };

    @NotNull
    protected final UnaryOperator<ModelAndView> navAndOverlayBlocksView = modelAndView -> {
        try {
            modelAndView.addObject("userEmail", CurrentUserUtil.email.get());
        } catch (@NotNull final AccessDeniedException exception) {
            modelAndView.addObject("userEmail", "");
        }
        return this.navBlockView.apply(modelAndView);
    };

    @NotNull
    protected final UnaryOperator<ModelAndView> navAndRoleBlocksView = modelAndView -> {
        try {
            modelAndView.addObject(
                    "userRole", CurrentUserUtil.userRoleType.get()
                            .getTitle()
            );
        } catch (@NotNull final AccessDeniedException exception) {
            modelAndView.addObject("userRole", "");
        }
        return this.navBlockView.apply(modelAndView);
    };

}