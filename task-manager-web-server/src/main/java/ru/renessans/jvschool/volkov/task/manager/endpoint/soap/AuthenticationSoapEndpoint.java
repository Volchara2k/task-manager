package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IAuthenticationSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
@Controller
@RequiredArgsConstructor
public class AuthenticationSoapEndpoint implements IAuthenticationSoapEndpoint {

    @NotNull
    private final IAuthenticationService authenticationService;

    @NotNull
    @Resource
    private final AuthenticationManager authenticationManagerBean;

    @WebMethod
    @WebResult(name = "loginFlag", partName = "loginFlag")
    @Override
    public boolean login(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password
    ) {
        @NotNull final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(login, password);
        @NotNull final Authentication authentication = this.authenticationManagerBean.authenticate(authenticationToken);
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);
        return authentication.isAuthenticated();
    }

    @WebMethod
    @WebResult(name = "logoutFlag", partName = "logoutFlag")
    @Override
    public boolean logout() {
        @Nullable final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication)) return false;
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(null);
        return true;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final User user = this.authenticationService.signUp(login, password);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUserWithFirstName(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final User user = this.authenticationService.signUp(login, password, firstName);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

}