package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import java.util.Collection;

@SuppressWarnings("unused")
public interface ITaskSoapEndpoint {

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @NotNull
    TaskDTO addTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @NotNull
    TaskDTO updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteTaskById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws InvalidParamException;

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    TaskDTO getTaskById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws InvalidParamException;

    @WebMethod
    @WebResult(name = "tasksDTO", partName = "tasksDTO")
    @NotNull
    Collection<TaskDTO> getAllTasks() throws InvalidParamException;

}