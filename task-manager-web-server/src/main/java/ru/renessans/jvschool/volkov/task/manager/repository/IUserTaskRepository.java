package ru.renessans.jvschool.volkov.task.manager.repository;

import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;

public interface IUserTaskRepository extends IUserOwnerRepository<Task> {
}