package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class NotFoundUserOwnerException extends AbstractException {

    @NotNull
    private static final String EMPTY_OBJECT = "Ожидаемый элемент пользователя не найден!";

    public NotFoundUserOwnerException() {
        super(EMPTY_OBJECT);
    }

}