package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum UserOwnerStatus {

    NOT_STARTED("Не начато"),

    IN_PROGRESS("В процессе"),

    COMPLETED("Выполнено");

    @NotNull
    private final String title;

    UserOwnerStatus(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

}