package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserProjectRepository;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

@Service
@Transactional(
        rollbackFor = Exception.class
)
public class UserProjectService extends AbstractUserOwnerService<Project> implements IUserProjectService {

    public UserProjectService(
            @NotNull final IUserProjectRepository userProjectRepository,
            @NotNull final IUserService userService
    ) {
        super(userProjectRepository, userService);
    }

    @NotNull
    @Override
    public Project addUserOwner(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) throws InvalidParamException, NotFoundUserOwnerException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidParamException("title");
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidParamException("description");
        @NotNull final Project project = new Project(userId, title, description);
        return super.addUserOwner(project);
    }

}