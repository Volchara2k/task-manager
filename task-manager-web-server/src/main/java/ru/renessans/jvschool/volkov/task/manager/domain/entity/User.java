package ru.renessans.jvschool.volkov.task.manager.domain.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_user")
@SuppressWarnings("JpaDataSourceORMInspection")
public final class User extends AbstractModel {

    @NotNull
    @Builder.Default
    @Column(
            unique = true,
            nullable = false,
            length = 25
    )
    private String login = "";

    @NotNull
    @Builder.Default
    @Column(
            nullable = false
    )
    private String passwordHash = "";

    @NotNull
    @Builder.Default
    @Column(
            nullable = false,
            length = 30
    )
    private String email = "";

    @NotNull
    @Builder.Default
    @Column(
            length = 25
    )
    private String firstName = "";

    @Nullable
    @Builder.Default
    @Column(
            length = 25
    )
    private String lastName = "";

    @Nullable
    @Builder.Default
    @Column(
            length = 25
    )
    private String middleName = "";

    @NotNull
    @Builder.Default
    @Column(nullable = false)
    private Boolean lockdown = false;

    @NotNull
    @Builder.Default
    @Column(
            nullable = false,
            length = 25
    )
    @Enumerated(EnumType.STRING)
    private UserRoleType userRoleType = UserRoleType.USER;

    @Nullable
    @Builder.Default
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @Builder.Default
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String password
    ) {
        setLogin(login);
        setPasswordHash(password);
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName
    ) {
        setLogin(login);
        setPasswordHash(password);
        setFirstName(firstName);
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final UserRoleType userRoleType
    ) {
        setLogin(login);
        setPasswordHash(password);
        setUserRoleType(userRoleType);
    }

}