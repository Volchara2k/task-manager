package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;

public interface IDataInterChangeService {

    boolean dataBinClear();

    boolean dataBase64Clear();

    boolean dataJsonClear();

    boolean dataXmlClear();

    boolean dataYamlClear();

    @NotNull
    DomainDTO exportDataBin() throws InvalidParamException;

    @NotNull
    DomainDTO exportDataBase64() throws InvalidParamException;

    @NotNull
    DomainDTO exportDataJson() throws InvalidParamException;

    @NotNull
    DomainDTO exportDataXml() throws InvalidParamException;

    @NotNull
    DomainDTO exportDataYaml() throws InvalidParamException;

    @NotNull
    DomainDTO importDataBin() throws InvalidParamException;

    @NotNull
    DomainDTO importDataBase64() throws InvalidParamException;

    @NotNull
    DomainDTO importDataJson() throws InvalidParamException;

    @NotNull
    DomainDTO importDataXml() throws InvalidParamException;

    @NotNull
    DomainDTO importDataYaml() throws InvalidParamException;

}