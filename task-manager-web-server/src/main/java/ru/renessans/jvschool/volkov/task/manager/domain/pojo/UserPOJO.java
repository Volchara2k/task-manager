package ru.renessans.jvschool.volkov.task.manager.domain.pojo;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class UserPOJO {

    @NotNull
    @CsvBindByName(column = "Идентификатор")
    private String id = "";

    @NotNull
    @CsvBindByName(column = "Логин")
    private String login = "";

    @NotNull
    @CsvBindByName(column = "Хеш-пароля")
    private String passwordHash = "";

    @NotNull
    @CsvBindByName(column = "Почта")
    private String email = "";

    @NotNull
    @CsvBindByName(column = "Имя")
    private String firstName = "";

    @Nullable
    @CsvBindByName(column = "Фамилия")
    private String lastName = "";

    @Nullable
    @CsvBindByName(column = "Отчество")
    private String middleName = "";

    @NotNull
    @CsvBindByName(column = "Заблокирован")
    private Boolean lockdown = false;

    @NotNull
    @CsvBindByName(column = "Права")
    private String userRoleType = "";

}