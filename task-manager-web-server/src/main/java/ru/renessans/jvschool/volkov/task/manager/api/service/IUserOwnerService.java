package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.AbstractModel;

import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused")
public interface IUserOwnerService<E extends AbstractModel> extends IDaoService<E> {

    @NotNull
    E addUserOwner(
            @Nullable E value
    ) throws NotFoundUserOwnerException, InvalidParamException;

    @NotNull
    E addUserOwner(
            @Nullable String userId,
            @Nullable String title,
            @Nullable String description
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @NotNull
    E updateUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String updatableTitle,
            @Nullable String updatableDescription
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @NotNull
    E updateUserOwnerById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String updatableTitle,
            @Nullable String updatableDescription
    ) throws InvalidParamException, NotFoundUserOwnerException;

    int deleteUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws InvalidParamException;

    int deleteUserOwnerById(
            @Nullable String userId,
            @Nullable String id
    ) throws InvalidParamException;

    int deleteUserOwnerByTitle(
            @Nullable String userId,
            @Nullable String title
    ) throws InvalidParamException;

    int deleteUserOwnerAll(
            @Nullable String userId
    ) throws InvalidParamException;

    @Nullable
    E getUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws InvalidParamException;

    @Nullable
    E getUserOwnerById(
            @Nullable String userId,
            @Nullable String id
    ) throws InvalidParamException;

    @Nullable
    E getUserOwnerByTitle(
            @Nullable String userId,
            @Nullable String title
    ) throws InvalidParamException;

    @NotNull
    Collection<E> getUserOwnerAll(
            @Nullable String userId
    ) throws InvalidParamException;

    @NotNull
    List<E> exportUserOwner();

    long countUserOwner(
            @Nullable String userId
    ) throws InvalidParamException;

}