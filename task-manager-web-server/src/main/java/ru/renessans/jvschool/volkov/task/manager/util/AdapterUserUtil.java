package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserUnlimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.domain.pojo.UserPOJO;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
public class AdapterUserUtil {

    @NotNull
    public Function<Collection<UserPOJO>, Collection<User>> forUsers = usersPOJO -> {
        @NotNull final Collection<User> destinations = new LinkedList<>();
        usersPOJO.forEach(userPOJO -> destinations.add(
                User.builder()
                        .id(userPOJO.getId())
                        .login(userPOJO.getLogin())
                        .passwordHash(userPOJO.getPasswordHash())
                        .email(userPOJO.getEmail())
                        .firstName(userPOJO.getFirstName())
                        .lastName(userPOJO.getLastName())
                        .middleName(userPOJO.getMiddleName())
                        .lockdown(userPOJO.getLockdown())
                        .userRoleType(
                                UserRoleType.valueOf(
                                        userPOJO.getUserRoleType()
                                )
                        )
                        .build()
        ));
        return destinations;
    };

    @NotNull
    public Function<User, UserLimitedDTO> forLimitedUserDTO = limitedUser -> UserLimitedDTO.builder()
            .id(limitedUser.getId())
            .login(limitedUser.getLogin())
            .firstName(limitedUser.getFirstName())
            .lastName(limitedUser.getLastName())
            .middleName(limitedUser.getMiddleName())
            .build();

    @NotNull
    public Function<UserLimitedDTO, User> forLimitedUser = limitedUserDTO -> User.builder()
            .id(
                    StringUtils.hasText(limitedUserDTO.getId())
                            ? limitedUserDTO.getId() : UUID.randomUUID().toString()
            )
            .login(limitedUserDTO.getLogin())
            .firstName(limitedUserDTO.getFirstName())
            .lastName(limitedUserDTO.getLastName())
            .middleName(limitedUserDTO.getMiddleName())
            .build();

    @NotNull
    public Function<User, UserUnlimitedDTO> forUnlimitedUserDTO = unlimitedUser -> UserUnlimitedDTO.builder()
            .id(unlimitedUser.getId())
            .login(unlimitedUser.getLogin())
            .passwordHash(unlimitedUser.getPasswordHash())
            .firstName(unlimitedUser.getFirstName())
            .lastName(unlimitedUser.getLastName())
            .middleName(unlimitedUser.getMiddleName())
            .userRoleType(unlimitedUser.getUserRoleType())
            .lockDown(unlimitedUser.getLockdown())
            .build();

    @NotNull
    public Function<UserUnlimitedDTO, User> forUnlimitedUser = unlimitedUser -> User.builder()
            .id(unlimitedUser.getId())
            .login(unlimitedUser.getLogin())
            .passwordHash(unlimitedUser.getPasswordHash())
            .firstName(unlimitedUser.getFirstName())
            .lastName(unlimitedUser.getLastName())
            .middleName(unlimitedUser.getMiddleName())
            .userRoleType(unlimitedUser.getUserRoleType())
            .lockdown(unlimitedUser.getLockDown())
            .build();

    @NotNull
    public Function<User, UserDetails> forUserDetail = user -> SecureUserDTO.detailBuilder()
            .userDetails(
                    SecureUserDTO.builder()
                            .username(user.getLogin())
                            .password(user.getPasswordHash())
                            .roles(
                                    Stream.of(user.getUserRoleType())
                                            .map(Enum::toString)
                                            .collect(Collectors.joining())
                            )
                            .accountLocked(user.getLockdown())
                            .build()
            )
            .id(user.getId())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .userRoleType(user.getUserRoleType())
            .build();

}