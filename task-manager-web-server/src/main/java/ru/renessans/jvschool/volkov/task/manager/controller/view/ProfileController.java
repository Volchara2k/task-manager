package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class ProfileController extends AbstractController {

    @NotNull
    @GetMapping("/profile")
    public ModelAndView profile() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("profile");
        return super.navAndOverlayBlocksView.apply(modelAndView);
    }

}