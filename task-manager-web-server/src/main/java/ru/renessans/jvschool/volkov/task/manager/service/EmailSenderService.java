package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEmailSenderService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAddresseeException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidContentException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidSendEmailException;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService implements IEmailSenderService {

    @NotNull
    private final JavaMailSender javaMailSender;

    @NotNull
    private final SimpleMailMessage simpleMailMessage;

    @Override
    public boolean sendTo(
            @Nullable final String[] to, @Nullable final String content
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException {
        log.info("send to {} elements pending", to.length);
        if (ValidRuleUtil.isNullOrEmpty(to)) {
            log.error("send to error {}", InvalidAddresseeException.class);
            throw new InvalidAddresseeException();
        }
        if (ValidRuleUtil.isNullOrEmpty(content)) {
            log.error("send to error {}", InvalidContentException.class);
            throw new InvalidContentException();
        }

        log.info("send to {} elements process", to.length);
        this.initialMessage(to, content);
        return this.sendingMessage();
    }

    private void initialMessage(
            @NotNull final String[] to, @NotNull final String content
    ) {
        this.simpleMailMessage.setTo(to);
        this.simpleMailMessage.setText(content);
    }

    private boolean sendingMessage(
    ) throws InvalidSendEmailException {
        try {
            log.info("sending message process");
            this.javaMailSender.send(
                    this.simpleMailMessage
            );
        } catch (@NotNull final Exception exception) {
            log.error("sending message error {}", InvalidSendEmailException.class);
            log.info(exception.getLocalizedMessage());
            throw new InvalidSendEmailException("Invalid sending message to email", exception);
        }

        log.info("sending message fine");
        return true;
    }

}