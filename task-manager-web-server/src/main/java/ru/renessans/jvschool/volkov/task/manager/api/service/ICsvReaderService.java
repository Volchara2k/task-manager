package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;

import java.io.IOException;
import java.util.Collection;

public interface ICsvReaderService<T> {

    @NotNull
    Collection<T> readFromResources(
    ) throws InvalidParamException, IOException;

    @NotNull
    Collection<T> readFromResources(
            @Nullable String pathname
    ) throws InvalidParamException, IOException;

}