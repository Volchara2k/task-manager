package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import java.util.Collection;

@SuppressWarnings("unused")
public interface IProjectSoapEndpoint {

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @NotNull
    ProjectDTO addProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    ProjectDTO updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    int deleteProjectById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws InvalidParamException;

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    ProjectDTO getProjectById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) throws InvalidParamException;

    @WebMethod
    @WebResult(name = "projectsDTO", partName = "projectsDTO")
    @NotNull
    Collection<ProjectDTO> getAllProjects() throws InvalidParamException;

}