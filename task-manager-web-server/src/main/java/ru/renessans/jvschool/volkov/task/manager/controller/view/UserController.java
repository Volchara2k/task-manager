package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController extends AbstractController {

    @GetMapping("/sign-in")
    public ModelAndView signIn() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("login");
        return super.navAndOverlayBlocksView.apply(modelAndView);
    }

    @GetMapping("/sign-up")
    public ModelAndView signUp() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("sign-up");
        return super.navAndOverlayBlocksView.apply(modelAndView);
    }

}