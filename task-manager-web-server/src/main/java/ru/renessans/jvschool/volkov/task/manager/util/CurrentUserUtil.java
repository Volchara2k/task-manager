package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import java.util.Objects;
import java.util.function.Supplier;

@UtilityClass
public class CurrentUserUtil {

    @NotNull
    public Supplier<String> userId = () -> fetchCurrentUser()
            .getId();

    @NotNull
    public Supplier<String> email = () -> fetchCurrentUser()
            .getEmail();

    @NotNull
    public Supplier<String> firstName = () -> fetchCurrentUser()
            .getFirstName();

    @NotNull
    public Supplier<UserRoleType> userRoleType = () -> Objects.requireNonNull(fetchCurrentUser()
            .getUserRoleType());

    @NotNull
    public SecureUserDTO fetchCurrentUser() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal instanceof SecureUserDTO) return (SecureUserDTO) principal;
        throw new AccessDeniedException("Необходимо авторизоваться!");
    }

}