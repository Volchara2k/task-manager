package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.domain.pojo.UserOwnerPOJO;

public interface IUserOwnerReaderService extends ICsvReaderService<UserOwnerPOJO> {
}