package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;

public interface IUserTaskService extends IUserOwnerService<Task> {
}