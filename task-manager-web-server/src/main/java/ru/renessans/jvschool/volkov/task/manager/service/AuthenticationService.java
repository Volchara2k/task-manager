package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        return this.userService.addUser(login, password);
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidParamException("firstName");
        return this.userService.addUser(login, password, firstName);
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRoleType userRoleType
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        if (Objects.isNull(userRoleType)) throw new InvalidParamException("userRoleType");
        return this.userService.addUser(login, password, userRoleType);
    }

}