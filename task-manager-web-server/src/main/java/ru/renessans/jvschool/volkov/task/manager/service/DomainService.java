package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterTaskUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IUserTaskService userTaskService;

    @NotNull
    private final IUserProjectService userProjectService;

    @NotNull
    @Override
    public DomainDTO dataImport(@Nullable final DomainDTO domain) throws InvalidParamException {
        if (Objects.isNull(domain)) throw new InvalidParamException("domain");
        this.disposeRecords();

        this.userService.importRecords(
                domain.getUsers()
                        .stream()
                        .map(userUnlimitedDTO -> AdapterUserUtil.forUnlimitedUser.apply(userUnlimitedDTO))
                        .collect(Collectors.toList())
        );

        this.userProjectService.importRecords(
                domain.getProjects()
                        .stream()
                        .map(projectDTO -> AdapterProjectUtil.forProject.apply(projectDTO))
                        .collect(Collectors.toList())
        );

        this.userTaskService.importRecords(
                domain.getTasks()
                        .stream()
                        .map(taskDTO -> AdapterTaskUtil.forTask.apply(taskDTO))
                        .collect(Collectors.toList())
        );

        return domain;
    }

    @NotNull
    @Override
    public DomainDTO dataExport(@Nullable final DomainDTO domain) throws InvalidParamException {
        if (Objects.isNull(domain)) throw new InvalidParamException("domain");

        domain.setUsers(
                this.userService.exportRecords()
                        .stream()
                        .map(user -> AdapterUserUtil.forUnlimitedUserDTO.apply(user))
                        .collect(Collectors.toList())
        );

        domain.setProjects(
                this.userProjectService.exportRecords()
                        .stream()
                        .map(project -> AdapterProjectUtil.forProjectDTO.apply(project))
                        .collect(Collectors.toList())
        );

        domain.setTasks(
                this.userTaskService.exportRecords()
                        .stream()
                        .map(task -> AdapterTaskUtil.forTaskDTO.apply(task))
                        .collect(Collectors.toList())
        );

        return domain;
    }

    private void disposeRecords() {
        this.userService.deleteAllRecords();
        this.userProjectService.deleteAllRecords();
        this.userTaskService.deleteAllRecords();
    }

}