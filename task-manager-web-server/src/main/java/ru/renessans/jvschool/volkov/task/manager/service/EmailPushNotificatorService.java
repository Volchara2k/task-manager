package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEmailPushNotificatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEmailSenderService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAddresseeException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidContentException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidSendEmailException;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

@Service
public class EmailPushNotificatorService extends AbstractEmailNotificatorService implements IEmailPushNotificatorService {

    @NotNull
    private final String pushContent;

    public EmailPushNotificatorService(
            @NotNull final IEmailSenderService emailSenderService,
            @Value("${mail.message.push}") @NotNull final String pushContent
    ) {
        super(emailSenderService);
        this.pushContent = pushContent;
    }

    @Override
    public boolean notifySubscribers(
    ) throws InvalidAddresseeException, InvalidContentException, InvalidSendEmailException {
        @NotNull final String email = CurrentUserUtil.email.get();
        return super.notifySubscribers(email, pushContent);
    }

}