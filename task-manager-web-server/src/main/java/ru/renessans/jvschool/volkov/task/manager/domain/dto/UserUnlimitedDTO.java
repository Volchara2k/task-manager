package ru.renessans.jvschool.volkov.task.manager.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserUnlimitedDTO extends UserLimitedDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "Password hash of user",
            example = "3213$daddy$23131321$faff",
            name = "passwordHash",
            required = true
    )
    @Size(
            max = 40,
            message = "Пароль не должен быть более 40 символов!"
    )
    private String passwordHash = "passwordHash";

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "Lock down of user",
            example = "false",
            name = "lockDown",
            required = true
    )
    private Boolean lockDown = false;

    @NotNull
    @Builder.Default
    @ApiModelProperty(
            value = "User role",
            example = "USER",
            name = "userRoleType",
            required = true
    )
    private UserRoleType userRoleType = UserRoleType.USER;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(super.getLogin());
        if (StringUtils.hasText(super.getFirstName()))
            result.append(", имя: ").append(super.getFirstName()).append("\n");
        if (StringUtils.hasText(super.getLastName()))
            result.append(", фамилия: ").append(super.getLastName()).append("\n");
        result.append("\nРоль: ").append(getUserRoleType().getTitle()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}