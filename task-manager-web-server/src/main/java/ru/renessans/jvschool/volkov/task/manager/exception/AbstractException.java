package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unused")
public abstract class AbstractException extends Exception {

    public AbstractException(
            @Nullable final String message
    ) {
        super(message);
    }

    public AbstractException(
            @NotNull final Throwable cause
    ) {
        super(cause);
    }

    public AbstractException(
            @Nullable final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

}