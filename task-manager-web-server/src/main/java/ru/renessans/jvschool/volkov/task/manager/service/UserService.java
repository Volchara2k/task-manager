package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserReaderService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.domain.pojo.UserPOJO;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Service
@SuppressWarnings("unused")
@Transactional(
        rollbackFor = Exception.class
)
public class UserService extends AbstractDaoService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IUserReaderService userReaderService;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IUserReaderService userReaderService,
            @NotNull final PasswordEncoder passwordEncoder
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.userReaderService = userReaderService;
        this.passwordEncoder = passwordEncoder;
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        return this.userRepository.save(user);
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidParamException("firstName");
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return this.userRepository.save(user);
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRoleType userRoleType
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidParamException("password");
        if (Objects.isNull(userRoleType)) throw new InvalidParamException("userRoleType");
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash, userRoleType);
        return this.userRepository.save(user);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public User getUserById(
            @Nullable final String id
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return super.getRecordById(id);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        return this.userRepository.getUserByLogin(login);
    }

    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public boolean existsUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        return this.userRepository.existsByLogin(login);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public UserRoleType getUserRole(
            @Nullable final String userId
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        @Nullable final User user = this.getUserById(userId);
        if (Objects.isNull(user)) return null;
        return user.getUserRoleType();
    }

    @NotNull
    @Override
    public User updateUserPasswordById(
            @Nullable final String id,
            @Nullable final String updatablePassword
    ) throws InvalidParamException, NotFoundUserException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        if (ValidRuleUtil.isNullOrEmpty(updatablePassword)) throw new InvalidParamException("updatablePassword");
        @NotNull final String passwordHash = this.passwordEncoder.encode(updatablePassword);
        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new NotFoundUserException();
        user.setPasswordHash(passwordHash);
        return super.save(user);
    }

    @NotNull
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) throws InvalidParamException, NotFoundUserException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidParamException("firstName");
        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new NotFoundUserException();
        user.setFirstName(firstName);
        return super.save(user);
    }

    @NotNull
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) throws InvalidParamException, NotFoundUserException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidParamException("firstName");
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new InvalidParamException("lastName");

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new NotFoundUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return super.save(user);
    }

    @NotNull
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException, NotFoundUserException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new NotFoundUserException();

        @NotNull final UserRoleType userRoleType = user.getUserRoleType();
        final boolean isAdminRole = userRoleType.isAdmin();
        if (isAdminRole) throw new AccessDeniedException("Операция недоступна!");

        user.setLockdown(true);
        return super.save(user);
    }

    @NotNull
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException, NotFoundUserException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new NotFoundUserException();

        @NotNull final UserRoleType userRoleType = user.getUserRoleType();
        final boolean isAdminRole = userRoleType.isAdmin();
        if (isAdminRole) throw new AccessDeniedException("Операция недоступна!");

        user.setLockdown(false);
        return super.save(user);
    }

    @Override
    public int deleteUserById(
            @Nullable final String id
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return super.deleteRecordById(id);
    }

    @Override
    public int deleteUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidParamException("login");
        return this.userRepository.deleteByLogin(login);
    }

    @PostConstruct
    @NotNull
    @Override
    public Collection<User> initialDemoUsers(
    ) throws InvalidParamException, IOException {
        final long existDataCount = this.userRepository.count();
        final boolean isNotExistData = existDataCount == 0;

        if (isNotExistData) {
            @NotNull final Collection<UserPOJO> usersPOJO = this.userReaderService.readFromResources();
            @NotNull final Collection<User> users = AdapterUserUtil.forUsers.apply(usersPOJO);
            return this.userRepository.saveAll(users);
        }

        return Collections.emptyList();
    }

}