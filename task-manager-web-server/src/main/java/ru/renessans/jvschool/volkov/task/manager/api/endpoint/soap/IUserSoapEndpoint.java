package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

@SuppressWarnings("unused")
public interface IUserSoapEndpoint {

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable UserLimitedDTO getUser() throws InvalidParamException;

    @WebMethod
    @WebResult(name = "userRole", partName = "userRole")
    @Nullable UserRoleType getUserRole() throws InvalidParamException;

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull UserLimitedDTO editProfile(
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName
    ) throws InvalidParamException, NotFoundUserException;

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull UserLimitedDTO editProfileWithLastName(
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName
    ) throws InvalidParamException, NotFoundUserException;

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull UserLimitedDTO updatePassword(
            @WebParam(name = "updatablePassword", partName = "updatablePassword") @Nullable String updatablePassword
    ) throws InvalidParamException, NotFoundUserException;

}