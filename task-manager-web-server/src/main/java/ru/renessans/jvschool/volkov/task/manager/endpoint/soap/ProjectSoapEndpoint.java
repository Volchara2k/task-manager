package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@WebService
@RequiredArgsConstructor
public class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    @NotNull
    private final IUserProjectService projectUserService;

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @NotNull
    @Override
    public ProjectDTO addProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Project request = AdapterProjectUtil.forProject.apply(projectDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        request.setUserId(userid);
        @NotNull final Project response = this.projectUserService.addUserOwner(request);
        return AdapterProjectUtil.forProjectDTO.apply(response);
    }

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    @Override
    public ProjectDTO updateProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) throws InvalidParamException, NotFoundUserOwnerException {
        @NotNull final Project request = AdapterProjectUtil.forProject.apply(projectDTO);
        @NotNull final String userid = CurrentUserUtil.userId.get();
        if (ValidRuleUtil.isNullOrEmpty(request.getUserId())) request.setUserId(userid);
        @NotNull final Project response = this.projectUserService.addUserOwner(request);
        return AdapterProjectUtil.forProjectDTO.apply(response);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return this.projectUserService.deleteUserOwnerById(userid, id);
    }

    @WebMethod
    @WebResult(name = "projectDTO", partName = "projectDTO")
    @Nullable
    @Override
    public ProjectDTO getProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final Project project = projectUserService.getUserOwnerById(userid, id);
        if (Objects.isNull(project)) return null;
        return AdapterProjectUtil.forProjectDTO.apply(project);
    }

    @WebMethod
    @WebResult(name = "projectsDTO", partName = "projectsDTO")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllProjects(
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return this.projectUserService.getUserOwnerAll(userid)
                .stream()
                .map(project -> AdapterProjectUtil.forProjectDTO.apply(project))
                .collect(Collectors.toList());
    }

}