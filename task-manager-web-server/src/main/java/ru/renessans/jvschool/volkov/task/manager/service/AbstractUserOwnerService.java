package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserOwnerService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserOwnerRepository;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.*;

@Transactional(
        rollbackFor = Exception.class
)
public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractDaoService<E> implements IUserOwnerService<E> {

    @NotNull
    private final IUserOwnerRepository<E> userOwnerRepository;

    @NotNull
    private final IUserService userService;

    protected AbstractUserOwnerService(
            @NotNull final IUserOwnerRepository<E> userOwnerRepository,
            @NotNull final IUserService userService
    ) {
        super(userOwnerRepository);
        this.userOwnerRepository = userOwnerRepository;
        this.userService = userService;
    }

    @NotNull
    @Override
    public E addUserOwner(
            @Nullable final E value
    ) throws NotFoundUserOwnerException, InvalidParamException {
        if (Objects.isNull(value)) throw new NotFoundUserOwnerException();
        @Nullable final String userId = value.getUserId();
        @Nullable final User user = this.userService.getUserById(userId);
        value.setUser(user);
        return super.save(value);
    }

    @NotNull
    @Override
    public E updateUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String updatableTitle,
            @Nullable final String updatableDescription
    ) throws InvalidParamException, NotFoundUserOwnerException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new InvalidParamException("index");
        if (ValidRuleUtil.isNullOrEmpty(updatableTitle)) throw new InvalidParamException("newTitle");
        if (ValidRuleUtil.isNullOrEmpty(updatableDescription)) throw new InvalidParamException("newDescription");

        @Nullable final E value = this.getUserOwnerByIndex(userId, index);
        if (Objects.isNull(value)) throw new NotFoundUserOwnerException();

        value.setTitle(updatableTitle);
        value.setDescription(updatableDescription);
        return super.save(value);
    }

    @NotNull
    @Override
    public E updateUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String updatableTitle,
            @Nullable final String updatableDescription
    ) throws InvalidParamException, NotFoundUserOwnerException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        if (ValidRuleUtil.isNullOrEmpty(updatableTitle)) throw new InvalidParamException("updatableTitle");
        if (ValidRuleUtil.isNullOrEmpty(updatableDescription)) throw new InvalidParamException("updatableDescription");

        @Nullable final E value = this.getUserOwnerById(userId, id);
        if (Objects.isNull(value)) throw new NotFoundUserOwnerException();

        value.setTitle(updatableTitle);
        value.setDescription(updatableDescription);
        return super.save(value);
    }

    @Override
    public int deleteUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.userOwnerRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public int deleteUserOwnerByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidParamException("title");
        return this.userOwnerRepository.deleteByUserIdAndTitle(userId, title);
    }

    @Override
    public int deleteUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new InvalidParamException("index");
        @NotNull final List<E> values = this.getUserOwnerAll(userId);
        if (index >= values.size()) return 0;
        @Nullable final E value = values.get(index);
        if (Objects.isNull(value)) return 0;
        return super.deleteRecordById(value.getId());
    }

    @Override
    public int deleteUserOwnerAll(
            @Nullable final String userId
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        return this.userOwnerRepository.deleteAllByUserId(userId);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public E getUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new InvalidParamException("index");
        @NotNull final List<E> values = this.getUserOwnerAll(userId);
        if (index >= values.size()) return null;
        return values.get(index);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public E getUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.userOwnerRepository.getUserOwnerById(userId, id);
    }

    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public E getUserOwnerByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidParamException("title");
        return this.userOwnerRepository.getUserOwnerByTitle(userId, title);
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public List<E> getUserOwnerAll(
            @Nullable final String userId
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        return this.userOwnerRepository.getUserOwnerAll(userId);
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public List<E> exportUserOwner() {
        return this.userOwnerRepository.exportUserOwner();
    }

    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public long countUserOwner(
            @Nullable final String userId
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        return this.userOwnerRepository.countUserOwner(userId);
    }

}