package ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;

import java.util.Collection;

@SuppressWarnings("unused")
public interface ITaskRestEndpoint {

    @NotNull
    @GetMapping("/tasks")
    @ApiOperation(
            value = "Get all tasks",
            notes = "Returns a complete list of tasks details by order of creation."
    )
    ResponseEntity<Collection<TaskDTO>> getAllTasks() throws InvalidParamException;

    @Nullable
    @GetMapping("/task/view/{id}")
    @ApiOperation(
            value = "Get task by ID",
            notes = "Returns task by unique ID. Unique ID required."
    )
    ResponseEntity<TaskDTO> getTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    ) throws InvalidParamException;

    @Nullable
    @RequestMapping(
            value = "/task/create",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Create task",
            notes = "Returns created task. Created task required."
    )
    ResponseEntity<TaskDTO> createTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Created task",
                    required = true
            )
            @RequestBody @NotNull TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

    @NotNull
    @DeleteMapping("/task/delete/{id}")
    @ApiOperation(
            value = "Delete task by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    ResponseEntity<Integer> deleteTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    ) throws InvalidParamException;

    @Nullable
    @RequestMapping(
            value = "/task/edit",
            method = RequestMethod.POST
    )
    @ApiOperation(
            value = "Edit task",
            notes = "Returns edited task. Edited task required."
    )
    ResponseEntity<TaskDTO> editTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Edited task",
                    required = true
            )
            @RequestBody @NotNull TaskDTO taskDTO
    ) throws InvalidParamException, NotFoundUserOwnerException;

}