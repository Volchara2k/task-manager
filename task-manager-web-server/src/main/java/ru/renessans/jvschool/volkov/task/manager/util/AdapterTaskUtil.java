package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.TimeFrame;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.*;
import java.util.function.Function;

@UtilityClass
public class AdapterTaskUtil {

    @NotNull
    public Function<Task, TaskDTO> forTaskDTO = task -> TaskDTO.builder()
            .id(task.getId())
            .userId(task.getUserId())
            .projectId(
                    (!Objects.isNull(task.getProject()) ?
                            task.getProject().getId() : null)
            )
            .title(task.getTitle())
            .description(task.getDescription())
            .timeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(task.getTimeFrame().getCreationDate())
                            .startDate(task.getTimeFrame().getStartDate())
                            .endDate(task.getTimeFrame().getEndDate())
                            .build()
            )
            .status(task.getStatus())
            .build();

    @NotNull
    public Function<TaskDTO, Task> forTask = taskDTO -> {
        if (ValidRuleUtil.isNullOrEmpty(taskDTO.getId())) taskDTO.setId(UUID.randomUUID().toString());
        if (Objects.isNull(taskDTO.getTimeFrame()) || Objects.isNull(taskDTO.getTimeFrame().getCreationDate()))
            taskDTO.setTimeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(new Date())
                            .startDate(taskDTO.getTimeFrame().getStartDate())
                            .endDate(taskDTO.getTimeFrame().getEndDate())
                            .build()
            );
        if (Objects.isNull(taskDTO.getStatus())) taskDTO.setStatus(UserOwnerStatus.NOT_STARTED);

        return Task.builder()
                .id(taskDTO.getId())
                .userId(taskDTO.getUserId())
                .project(
                        (!ValidRuleUtil.isNullOrEmpty(taskDTO.getProjectId()) ?
                                new Project(taskDTO.getProjectId()) : null)
                )
                .title(taskDTO.getTitle())
                .description(taskDTO.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(Objects.requireNonNull(taskDTO.getTimeFrame().getCreationDate()))
                                .startDate(taskDTO.getTimeFrame().getStartDate())
                                .endDate(taskDTO.getTimeFrame().getEndDate())
                                .build()
                )
                .status(taskDTO.getStatus())
                .build();
    };

}