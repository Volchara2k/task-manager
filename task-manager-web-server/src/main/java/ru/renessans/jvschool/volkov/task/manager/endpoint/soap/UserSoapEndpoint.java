package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IUserSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterUserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
@Controller
@RequiredArgsConstructor
public class UserSoapEndpoint implements IUserSoapEndpoint {

    @NotNull
    private final IUserService userService;

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUser(
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @Nullable final User user = this.userService.getUserById(userid);
        if (Objects.isNull(user)) return null;
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @WebMethod
    @WebResult(name = "userRole", partName = "userRole")
    @Nullable
    @Override
    public UserRoleType getUserRole(
    ) throws InvalidParamException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        return this.userService.getUserRole(userid);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    @Override
    public UserLimitedDTO editProfile(
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @NotNull final User user = this.userService.editUserProfileById(userid, firstName);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    @Override
    public UserLimitedDTO editProfileWithLastName(
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @NotNull final User user = this.userService.editUserProfileById(userid, firstName, lastName);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @NotNull
    @Override
    public UserLimitedDTO updatePassword(
            @WebParam(name = "updatablePassword", partName = "updatablePassword") @Nullable final String updatablePassword
    ) throws InvalidParamException, NotFoundUserException {
        @NotNull final String userid = CurrentUserUtil.userId.get();
        @NotNull final User user = this.userService.updateUserPasswordById(userid, updatablePassword);
        return AdapterUserUtil.forLimitedUserDTO.apply(user);
    }

}