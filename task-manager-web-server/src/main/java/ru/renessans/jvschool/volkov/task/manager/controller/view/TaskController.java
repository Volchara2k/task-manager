package ru.renessans.jvschool.volkov.task.manager.controller.view;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.annotation.Auth;
import ru.renessans.jvschool.volkov.task.manager.api.controller.ITaskController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterProjectUtil;
import ru.renessans.jvschool.volkov.task.manager.util.AdapterTaskUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@SuppressWarnings("SpringMVCViewInspection")
public class TaskController extends AbstractController implements ITaskController {

    @NotNull
    private final IUserTaskService userTaskService;

    @NotNull
    private final IUserProjectService userProjectService;

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/tasks")
    @Override
    public ModelAndView list(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("tasks");
        @NotNull final Collection<Task> tasks = this.userTaskService.getUserOwnerAll(userDTO.getId());
        modelAndView.addObject("tasks", tasks);
        return super.navBlockView.apply(modelAndView);
    }

    @Auth
    @NotNull
    @GetMapping("/task/create")
    @Override
    public ModelAndView create(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task-create");
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userDTO.getId());
        return this.settableModelAndView(userDTO, modelAndView, taskDTO);
    }

    @Auth
    @NotNull
    @SneakyThrows
    @PostMapping("/task/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @NotNull final Task task = AdapterTaskUtil.forTask.apply(taskDTO);
        this.userTaskService.addUserOwner(task);
        return new ModelAndView("redirect:/tasks");
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @Override
    public ModelAndView view(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.userTaskService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new NotFoundUserOwnerException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("task-view");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/task/delete/{id}")
    @Override
    public ModelAndView delete(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        this.userTaskService.deleteUserOwnerById(userDTO.getId(), id);
        return new ModelAndView("redirect:/tasks");
    }

    @Auth
    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.userTaskService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new NotFoundUserOwnerException();
        @Nullable final TaskDTO taskDTO = AdapterTaskUtil.forTaskDTO.apply(task);
        if (Objects.isNull(taskDTO)) throw new NotFoundUserOwnerException();

        @NotNull final ModelAndView modelAndView = new ModelAndView("task-updatable");
        return this.settableModelAndView(userDTO, modelAndView, taskDTO);
    }

    @Auth
    @NotNull
    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @NotNull final Task task = AdapterTaskUtil.forTask.apply(taskDTO);
        this.userTaskService.addUserOwner(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task/view/{id}");
        modelAndView.addObject("id", task.getId());
        return modelAndView;
    }

    @SneakyThrows
    @NotNull
    private ModelAndView settableModelAndView(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @NotNull final ModelAndView modelAndView,
            @Nullable final TaskDTO taskDTO
    ) {
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", UserOwnerStatus.values());
        @NotNull final Collection<ProjectDTO> projects =
                this.userProjectService.getUserOwnerAll(userDTO.getId())
                        .stream()
                        .map(project -> AdapterProjectUtil.forProjectDTO.apply(project))
                        .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return super.navAndOverlayBlocksView.apply(modelAndView);
    }

}