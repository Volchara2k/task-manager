package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.*;

import javax.xml.ws.Endpoint;

@Configuration
@SuppressWarnings("unused")
public class WebSoapConfiguration {

    @Bean
    @NotNull
    public Endpoint adminEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final IAdminSoapEndpoint adminSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, adminSoapEndpoint);
        endpoint.publish("/AdminEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint authenticationEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final IAuthenticationSoapEndpoint authenticationSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authenticationSoapEndpoint);
        endpoint.publish("/AuthenticationEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint dataInterChangeEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final IDataInterChangeSoapEndpoint dataInterChangeSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, dataInterChangeSoapEndpoint);
        endpoint.publish("/DataInterChangeEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final IProjectSoapEndpoint projectSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final ITaskSoapEndpoint taskSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint userEndpointRegistry(
            @NotNull final SpringBus cxf,
            @NotNull final IUserSoapEndpoint userSoapEndpoint
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, userSoapEndpoint);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }

}