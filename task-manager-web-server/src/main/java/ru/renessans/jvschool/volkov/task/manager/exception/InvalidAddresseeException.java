package ru.renessans.jvschool.volkov.task.manager.exception;

public final class InvalidAddresseeException extends AbstractException {

    public InvalidAddresseeException() {
        super("Обнаружены недопустимые адресанты");
    }

}