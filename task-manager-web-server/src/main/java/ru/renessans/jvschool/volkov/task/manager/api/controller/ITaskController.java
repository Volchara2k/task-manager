package ru.renessans.jvschool.volkov.task.manager.api.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.annotation.Auth;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;

@SuppressWarnings("unused")
public interface ITaskController {

    @Auth
    @NotNull
    @GetMapping("/tasks")
    ModelAndView list(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @Auth
    @NotNull
    @GetMapping("/task/create")
    ModelAndView create(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @Auth
    @NotNull
    @PostMapping("/task/create")
    ModelAndView create(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

    @Auth
    @NotNull
    @GetMapping("/task/view/{id}")
    ModelAndView view(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @GetMapping("/task/delete/{id}")
    ModelAndView delete(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @GetMapping("/task/edit/{id}")
    ModelAndView edit(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @Auth
    @NotNull
    @PostMapping("/task/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

}