package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DashboardController extends AbstractController {

    @GetMapping("/")
    public ModelAndView dashboard() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("index");
        return super.navAndRoleBlocksView.apply(modelAndView);
    }

}