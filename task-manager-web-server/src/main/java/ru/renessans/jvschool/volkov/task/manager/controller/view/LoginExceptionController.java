package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class LoginExceptionController {

    @GetMapping("/sign-in-error")
    public String loginException(@NotNull final Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

}