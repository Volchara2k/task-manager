package ru.renessans.jvschool.volkov.task.manager.exception.handler;

import org.jetbrains.annotations.NotNull;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ApiErrorResponseDTO;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice("ru.renessans.jvschool.volkov.task.manager.endpoint.rest")
public class RestExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @NotNull
    @Order(1)
    @ExceptionHandler({
            InvalidParamException.class,
            ConstraintViolationException.class
    })
    public ResponseEntity<ApiErrorResponseDTO> handleBadRequest(
            @NotNull final Exception exception
    ) {
        return this.buildResponse(HttpStatus.BAD_REQUEST, exception);
    }

    @NotNull
    @Order(2)
    @ExceptionHandler({
            DisabledException.class,
            LockedException.class,
            BadCredentialsException.class
    })
    public ResponseEntity<ApiErrorResponseDTO> handleUnauthorized(
            @NotNull final Exception exception
    ) {
        return this.buildResponse(HttpStatus.UNAUTHORIZED, exception);
    }

    @NotNull
    @Order(3)
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiErrorResponseDTO> handleForbidden(
            @NotNull final Exception exception
    ) {
        return this.buildResponse(HttpStatus.FORBIDDEN, exception);
    }

    @NotNull
    @Order(4)
    @ExceptionHandler({
            NotFoundUserOwnerException.class,
            NotFoundUserException.class
    })
    public ResponseEntity<ApiErrorResponseDTO> handleNotFound(
            @NotNull final Exception exception
    ) {
        return this.buildResponse(HttpStatus.NOT_FOUND, exception);
    }

    @NotNull
    @Order(5)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorResponseDTO> defaultHandler(
            @NotNull final Exception exception
    ) {
        return this.buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    @NotNull
    private ResponseEntity<ApiErrorResponseDTO> buildResponse(
            @NotNull final HttpStatus httpStatus,
            @NotNull final Exception exception
    ) {
        @NotNull final ApiErrorResponseDTO apiErrorResponse = new ApiErrorResponseDTO(httpStatus, exception);
        return new ResponseEntity<>(apiErrorResponse, httpStatus);
    }

}
