package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.exception.NotFoundUserOwnerException;
import ru.renessans.jvschool.volkov.task.manager.domain.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserTaskRepository;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

@Service
@Transactional(
        rollbackFor = Exception.class
)
public class UserTaskService extends AbstractUserOwnerService<Task> implements IUserTaskService {

    public UserTaskService(
            @NotNull final IUserTaskRepository userTaskRepository,
            @NotNull final IUserService userService
    ) {
        super(userTaskRepository, userService);
    }

    @NotNull
    @Override
    public Task addUserOwner(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) throws InvalidParamException, NotFoundUserOwnerException {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidParamException("userId");
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidParamException("title");
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidParamException("description");
        @NotNull final Task task = new Task(userId, title, description);
        return super.addUserOwner(task);
    }

}