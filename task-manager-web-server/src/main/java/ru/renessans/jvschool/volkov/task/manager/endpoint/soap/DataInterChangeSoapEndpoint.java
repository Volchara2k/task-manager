package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.annotation.Admin;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IDataInterChangeSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.DomainDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@Controller
@RequiredArgsConstructor
public class DataInterChangeSoapEndpoint implements IDataInterChangeSoapEndpoint {

    @NotNull
    private final IDataInterChangeService dataInterChangeService;

    @Admin
    @WebMethod
    @WebResult(name = "clearBinDataFlag", partName = "clearBinDataFlag")
    @Override
    public boolean dataBinClear() {
        return this.dataInterChangeService.dataBinClear();
    }

    @Admin
    @WebMethod
    @WebResult(name = "clearBase64DataFlag", partName = "clearBase64DataFlag")
    @Override
    public boolean dataBase64Clear() {
        return this.dataInterChangeService.dataBase64Clear();
    }

    @Admin
    @WebMethod
    @WebResult(name = "clearJsonDataFlag", partName = "clearJsonDataFlag")
    @Override
    public boolean dataJsonClear() {
        return this.dataInterChangeService.dataJsonClear();
    }

    @Admin
    @WebMethod
    @WebResult(name = "clearXmlDataFlag", partName = "clearXmlDataFlag")
    @Override
    public boolean dataXmlClear() {
        return this.dataInterChangeService.dataXmlClear();
    }

    @Admin
    @WebMethod
    @WebResult(name = "clearYamlDataFlag", partName = "clearYamlDataFlag")
    @Override
    public boolean dataYamlClear() {
        return this.dataInterChangeService.dataYamlClear();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBin() throws InvalidParamException {
        return this.dataInterChangeService.exportDataBin();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBase64() throws InvalidParamException {
        return this.dataInterChangeService.exportDataBase64();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataJson() throws InvalidParamException {
        return this.dataInterChangeService.exportDataJson();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataXml() throws InvalidParamException {
        return this.dataInterChangeService.exportDataXml();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataYaml() throws InvalidParamException {
        return this.dataInterChangeService.exportDataYaml();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBin() throws InvalidParamException {
        return this.dataInterChangeService.importDataBin();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBase64() throws InvalidParamException {
        return this.dataInterChangeService.importDataBase64();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataJson() throws InvalidParamException {
        return this.dataInterChangeService.importDataJson();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataXml() throws InvalidParamException {
        return this.dataInterChangeService.importDataXml();
    }

    @Admin
    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataYaml() throws InvalidParamException {
        return this.dataInterChangeService.importDataYaml();
    }

}