package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSessionProvider;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

public final class CurrentSessionRepositoryTest {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository = new CurrentSessionRepository();

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.currentSessionRepository);
    }

    @Test(expected = InvalidSessionException.class)
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    public void testNegativeDelete() {
        Assert.assertNotNull(this.currentSessionRepository);
        this.currentSessionRepository.delete();
    }

    @Test
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testSet() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);

        @NotNull final SessionDTO setSession = this.currentSessionRepository.set(sessionDTO);
        Assert.assertNotNull(setSession);
        Assert.assertEquals(sessionDTO.getId(), setSession.getId());
        Assert.assertEquals(sessionDTO.getTimestamp(), setSession.getTimestamp());
        Assert.assertEquals(sessionDTO.getUserId(), setSession.getUserId());
    }

    @Test
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGet() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);
        @NotNull final SessionDTO setSession = this.currentSessionRepository.set(sessionDTO);
        Assert.assertNotNull(setSession);

        @Nullable final SessionDTO session = this.currentSessionRepository.get();
        Assert.assertNotNull(session);
        Assert.assertEquals(setSession.getId(), session.getId());
        Assert.assertEquals(setSession.getTimestamp(), session.getTimestamp());
        Assert.assertEquals(setSession.getUserId(), session.getUserId());
        Assert.assertEquals(setSession.getSignature(), session.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testDelete() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);
        @NotNull final SessionDTO setSession = this.currentSessionRepository.set(sessionDTO);
        Assert.assertNotNull(setSession);

        @Nullable final SessionDTO deleteSession = this.currentSessionRepository.delete();
        Assert.assertNotNull(deleteSession);
        Assert.assertEquals(setSession.getId(), deleteSession.getId());
        Assert.assertEquals(setSession.getTimestamp(), deleteSession.getTimestamp());
        Assert.assertEquals(setSession.getUserId(), deleteSession.getUserId());
        Assert.assertEquals(setSession.getSignature(), deleteSession.getSignature());
    }

}