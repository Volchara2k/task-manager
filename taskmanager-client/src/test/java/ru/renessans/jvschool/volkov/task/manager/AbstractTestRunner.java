package ru.renessans.jvschool.volkov.task.manager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractRepositoryImplementationRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractServiceImplementationRunner;
import ru.renessans.jvschool.volkov.task.manager.runner.AbstractUtilityImplementationRunner;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AbstractRepositoryImplementationRunner.class,
                AbstractServiceImplementationRunner.class,
                AbstractUtilityImplementationRunner.class
        }
)
public abstract class AbstractTestRunner {
}