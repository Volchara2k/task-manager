package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;
import ru.renessans.jvschool.volkov.task.manager.util.SystemUtilTest;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtilTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(UtilityImplementation.class)
@Suite.SuiteClasses(
        {
                SystemUtilTest.class,
                ValidRuleUtilTest.class
        }
)
public abstract class AbstractUtilityImplementationRunner {
}