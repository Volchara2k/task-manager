package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class SessionEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Before
    public void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.sessionEndpoint);
    }

    @Test
    public void testVerifyValidSessionState() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final SessionValidState verifyValidSessionStateResponse =
                this.sessionEndpoint.verifyValidSessionState(openSessionResponse);
        Assert.assertNotNull(verifyValidSessionStateResponse);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionStateResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testVerifyValidPermissionState() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final PermissionValidState permissionValidStateResponse =
                this.sessionEndpoint.verifyValidPermissionState(openSessionResponse, UserRole.ADMIN);
        Assert.assertNotNull(permissionValidStateResponse);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidStateResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}