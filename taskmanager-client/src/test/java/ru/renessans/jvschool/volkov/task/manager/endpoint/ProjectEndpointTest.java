package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;
import java.util.UUID;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Before
    public void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
    }

    @Test
    public void testAddProject() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProjectResponse = this.projectEndpoint.addProject(
                openSessionResponse, title, description
        );
        Assert.assertNotNull(addProjectResponse);
        Assert.assertEquals(title, addProjectResponse.getTitle());
        Assert.assertEquals(description, addProjectResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdateProjectByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProjectResponse = this.projectEndpoint.updateProjectByIndex(
                openSessionResponse, 0, title, description
        );
        Assert.assertNotNull(updateProjectResponse);
        Assert.assertEquals(title, updateProjectResponse.getTitle());
        Assert.assertEquals(description, updateProjectResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdateProjectById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final ProjectDTO project = this.projectEndpoint.getProjectByIndex(openSessionResponse, 0);
        Assert.assertNotNull(project);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProjectResponse = this.projectEndpoint.updateProjectById(
                openSessionResponse, project.getId(), title, description
        );
        Assert.assertNotNull(updateProjectResponse);
        Assert.assertEquals(project.getId(), updateProjectResponse.getId());
        Assert.assertEquals(project.getUserId(), updateProjectResponse.getUserId());
        Assert.assertEquals(title, updateProjectResponse.getTitle());
        Assert.assertEquals(description, updateProjectResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteProjectById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProject);

        final int deleteProjectResponse = this.projectEndpoint.deleteProjectById(openSessionResponse, addProject.getId());
        Assert.assertEquals(1, deleteProjectResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteProjectByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProject);
        @NotNull final ProjectDTO getTask = this.projectEndpoint.getProjectByIndex(openSessionResponse, 0);
        Assert.assertNotNull(getTask);

        final int deleteProjectResponse = this.projectEndpoint.deleteProjectByIndex(openSessionResponse, 0);
        Assert.assertEquals(1, deleteProjectResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteProjectByTitle() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProject);

        final int deleteProjectResponse = this.projectEndpoint.deleteProjectByTitle(openSessionResponse, addProject.getTitle());
        Assert.assertEquals(1, deleteProjectResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteAllProjects() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProject);

        final int deleteProjectsResponse = this.projectEndpoint.deleteAllProjects(openSessionResponse);
        Assert.assertNotEquals(0, deleteProjectsResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetProjectById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProjectResponse = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProjectResponse);

        @Nullable final ProjectDTO projectResponse = this.projectEndpoint.getProjectById(openSessionResponse, addProjectResponse.getId());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(addProjectResponse.getId(), projectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(addProjectResponse.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(addProjectResponse.getDescription(), projectResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetProjectByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProjectResponse = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProjectResponse);

        @NotNull final ProjectDTO projectResponse = this.projectEndpoint.getProjectByIndex(openSessionResponse, 0);
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(openSessionResponse.getUserId(), projectResponse.getUserId());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetProjectByTitle() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProjectResponse = this.projectEndpoint.addProject(openSessionResponse, title, description);
        Assert.assertNotNull(addProjectResponse);

        @Nullable final ProjectDTO projectResponse = this.projectEndpoint.getProjectByTitle(openSessionResponse, addProjectResponse.getTitle());
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(addProjectResponse.getId(), projectResponse.getId());
        Assert.assertEquals(addProjectResponse.getUserId(), projectResponse.getUserId());
        Assert.assertEquals(addProjectResponse.getTitle(), projectResponse.getTitle());
        Assert.assertEquals(addProjectResponse.getDescription(), projectResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllProjects() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final Collection<ProjectDTO> projectsResponse = this.projectEndpoint.getAllProjects(openSessionResponse);
        Assert.assertNotNull(projectsResponse);
        Assert.assertNotEquals(0, projectsResponse.size());
        final boolean isUserProjects = projectsResponse.stream().allMatch(entity -> openSessionResponse.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserProjects);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}