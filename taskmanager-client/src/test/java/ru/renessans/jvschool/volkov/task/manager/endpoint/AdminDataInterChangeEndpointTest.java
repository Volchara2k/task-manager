package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class AdminDataInterChangeEndpointTest {

    @NotNull
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    private AdminDataInterChangeEndpoint adminDataInterChangeEndpoint;

    @AfterAll
    @SneakyThrows
    public static void deleteFilesAfter() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
    }

    @Before
    public void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
    }

    @BeforeEach
    @SneakyThrows
    public void createFilesBefore() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.createFile(new File(DemoDataConst.BIN_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.createFile(new File(DemoDataConst.BASE64_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.createFile(new File(DemoDataConst.JSON_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.createFile(new File(DemoDataConst.XML_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
        Files.createFile(new File(DemoDataConst.YAML_LOCATE).toPath());
    }

    @Test
    public void testDataBinClear() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = this.adminDataInterChangeEndpoint.dataBinClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDataBase64Clear() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = this.adminDataInterChangeEndpoint.dataBase64Clear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDataJsonClear() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = this.adminDataInterChangeEndpoint.dataJsonClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDataXmlClear() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = this.adminDataInterChangeEndpoint.dataXmlClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDataYamlClear() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final boolean clearResponse = this.adminDataInterChangeEndpoint.dataYamlClear(openSessionResponse);
        Assert.assertTrue(clearResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testExportDataBin() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataBin(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testExportDataBase64() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataBase64(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testExportDataJson() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testExportDataXml() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataXml(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testExportDataYaml() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataYaml(openSessionResponse);
        Assert.assertNotNull(exportResponse);
        Assert.assertNotEquals(0, exportResponse.getProjects().size());
        Assert.assertNotEquals(0, exportResponse.getTasks().size());
        Assert.assertNotEquals(0, exportResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testImportDataBin() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = this.adminDataInterChangeEndpoint.importDataBin(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testImportDataBase64() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataBase64(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = this.adminDataInterChangeEndpoint.importDataBase64(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testImportDataJson() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataJson(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = this.adminDataInterChangeEndpoint.importDataJson(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testImportDataXml() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataXml(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = this.adminDataInterChangeEndpoint.importDataXml(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testImportDataYaml() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final DomainDTO exportResponse = this.adminDataInterChangeEndpoint.exportDataYaml(openSessionResponse);
        Assert.assertNotNull(exportResponse);

        @NotNull final DomainDTO importResponse = this.adminDataInterChangeEndpoint.importDataYaml(openSessionResponse);
        Assert.assertNotNull(importResponse);
        Assert.assertNotEquals(0, importResponse.getProjects().size());
        Assert.assertNotEquals(0, importResponse.getTasks().size());
        Assert.assertNotEquals(0, importResponse.getUsers().size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}