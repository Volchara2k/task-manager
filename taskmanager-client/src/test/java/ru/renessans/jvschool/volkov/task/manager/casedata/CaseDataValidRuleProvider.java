package ru.renessans.jvschool.volkov.task.manager.casedata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@SuppressWarnings("unused")
public final class CaseDataValidRuleProvider {

    public Object[] invalidCollectionsCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{
                        true,
                        Arrays.asList(
                                null,
                                null,
                                null
                        )
                },
                new Object[]{true, Collections.emptyList()},
                new Object[]{true, new ArrayList<>()}
        };
    }

    public Object[] validCollectionsCaseData() {
        return new Object[]{
                new Object[]{
                        false,
                        Arrays.asList(
                                "",
                                "",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "null",
                                "not null",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                null,
                                "wolf",
                                null
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "string",
                                "data    ",
                                ", c..ca"
                        )
                }
        };
    }

}