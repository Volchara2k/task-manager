
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataJsonClearResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dataJsonClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clearJsonDataFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataJsonClearResponse", propOrder = {
    "clearJsonDataFlag"
})
public class DataJsonClearResponse {

    protected boolean clearJsonDataFlag;

    /**
     * Gets the value of the clearJsonDataFlag property.
     * 
     */
    public boolean isClearJsonDataFlag() {
        return clearJsonDataFlag;
    }

    /**
     * Sets the value of the clearJsonDataFlag property.
     * 
     */
    public void setClearJsonDataFlag(boolean value) {
        this.clearJsonDataFlag = value;
    }

}
