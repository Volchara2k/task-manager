package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IAdminSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IDataInterChangeSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.*;

@SuppressWarnings("unused")
public interface IApiConfiguration {

    @NotNull
    AdminSoapEndpointService adminSoapEndpointService();

    @NotNull
    AuthenticationSoapEndpointService authenticationEndpointService();

    @NotNull
    DataInterChangeSoapEndpointService dataInterChangeSoapEndpointService();

    @NotNull
    ProjectSoapEndpointService projectEndpointService();

    @NotNull
    TaskSoapEndpointService taskEndpointService();

    @NotNull
    UserSoapEndpointService userSoapEndpointService();

    @NotNull
    IAdminSoapEndpoint adminSoapEndpoint(
            @NotNull AdminSoapEndpointService adminSoapEndpointService
    );

    @NotNull
    AuthenticationSoapEndpoint authenticationEndpoint(
            @NotNull AuthenticationSoapEndpointService authenticationSoapEndpointService
    );

    @NotNull
    IDataInterChangeSoapEndpoint dataInterChangeSoapEndpoint(
            @NotNull DataInterChangeSoapEndpointService dataInterChangeSoapEndpointService
    );

    @NotNull
    ProjectSoapEndpoint projectEndpoint(
            @NotNull ProjectSoapEndpointService projectEndpointService
    );

    @NotNull
    TaskSoapEndpoint taskEndpoint(
            @NotNull TaskSoapEndpointService taskEndpointService
    );

    @NotNull
    UserSoapEndpoint userSoapEndpointService(
            @NotNull UserSoapEndpointService userSoapEndpointService
    );

}