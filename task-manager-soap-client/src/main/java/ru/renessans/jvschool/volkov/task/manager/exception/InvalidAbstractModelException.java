package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidAbstractModelException extends AbstractException {

    @NotNull
    private static final String EMPTY_OWNER = "Параметр \"сущность\" отсутствует!\n";

    public InvalidAbstractModelException() {
        super(EMPTY_OWNER);
    }

}