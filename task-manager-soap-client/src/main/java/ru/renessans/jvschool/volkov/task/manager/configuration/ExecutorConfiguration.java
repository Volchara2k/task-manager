package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IExecutorConfiguration;

import java.util.concurrent.Executor;

@EnableAsync
public class ExecutorConfiguration implements IExecutorConfiguration {

    @Bean
    @NotNull
    @Override
    public Executor executor() {
        @NotNull final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setMaxPoolSize(2);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setThreadNamePrefix("Listener thread-");
        taskExecutor.initialize();
        return taskExecutor;
    }

}