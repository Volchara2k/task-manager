
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userRoleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="userRoleType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ADMIN"/&gt;
 *     &lt;enumeration value="USER"/&gt;
 *     &lt;enumeration value="MANAGER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "userRoleType")
@XmlEnum
public enum UserRoleType {

    ADMIN,
    USER,
    MANAGER;

    public String value() {
        return name();
    }

    public static UserRoleType fromValue(String v) {
        return valueOf(v);
    }

}
