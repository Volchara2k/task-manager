package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.utility.ScannerUtil;

import java.util.function.Supplier;

@UtilityClass
public class ConsoleUtil {

    @NotNull
    public Supplier<String> lineFactory = () -> {
        System.out.print("Введите строковые данные: ");
        return ScannerUtil.getLine();
    };

    @NotNull
    public Supplier<Integer> integerFactory = () -> {
        System.out.print("Введите индекс: ");
        return ScannerUtil.getInteger();
    };

}