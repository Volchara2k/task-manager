package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AuthenticationSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ConsoleUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserSignUpListener extends AbstractSecurityListener {

    @NotNull
    private static final String CMD_SIGN_UP = "sign-up";

    @NotNull
    private static final String DESC_SIGN_UP = "зарегистрироваться в системе";

    @NotNull
    private static final String NOTIFY_SIGN_UP =
            "Происходит попытка инициализации регистрации пользователя. \n" +
                    "Для регистрации пользователя в системе введите логин и пароль: ";

    public UserSignUpListener(
            @NotNull final AuthenticationSoapEndpoint sessionEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(sessionEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_SIGN_UP;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SIGN_UP;
    }

    @Async
    @Override
    @EventListener(condition = "@userSignUpListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_SIGN_UP);
        @NotNull final String login = ConsoleUtil.lineFactory.get();
        @NotNull final String password = ConsoleUtil.lineFactory.get();
        @Nullable final UserLimitedDTO signUpResponse = super.authenticationEndpoint.signUpUser(login, password);
        ViewUtil.print(signUpResponse);
    }

}