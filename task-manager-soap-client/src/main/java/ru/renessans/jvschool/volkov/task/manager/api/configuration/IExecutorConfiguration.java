package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executor;

public interface IExecutorConfiguration {

    @Bean
    @NotNull Executor executor();

}