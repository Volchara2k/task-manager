
package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.renessans.jvschool.volkov.task.manager.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EditProfile_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "editProfile");
    private final static QName _EditProfileResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "editProfileResponse");
    private final static QName _EditProfileWithLastName_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "editProfileWithLastName");
    private final static QName _EditProfileWithLastNameResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "editProfileWithLastNameResponse");
    private final static QName _GetUser_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getUser");
    private final static QName _GetUserResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getUserResponse");
    private final static QName _GetUserRole_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getUserRole");
    private final static QName _GetUserRoleResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getUserRoleResponse");
    private final static QName _UpdatePassword_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "updatePassword");
    private final static QName _UpdatePasswordResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "updatePasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.renessans.jvschool.volkov.task.manager.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EditProfile }
     * 
     */
    public EditProfile createEditProfile() {
        return new EditProfile();
    }

    /**
     * Create an instance of {@link EditProfileResponse }
     * 
     */
    public EditProfileResponse createEditProfileResponse() {
        return new EditProfileResponse();
    }

    /**
     * Create an instance of {@link EditProfileWithLastName }
     * 
     */
    public EditProfileWithLastName createEditProfileWithLastName() {
        return new EditProfileWithLastName();
    }

    /**
     * Create an instance of {@link EditProfileWithLastNameResponse }
     * 
     */
    public EditProfileWithLastNameResponse createEditProfileWithLastNameResponse() {
        return new EditProfileWithLastNameResponse();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link GetUserRole }
     * 
     */
    public GetUserRole createGetUserRole() {
        return new GetUserRole();
    }

    /**
     * Create an instance of {@link GetUserRoleResponse }
     * 
     */
    public GetUserRoleResponse createGetUserRoleResponse() {
        return new GetUserRoleResponse();
    }

    /**
     * Create an instance of {@link UpdatePassword }
     * 
     */
    public UpdatePassword createUpdatePassword() {
        return new UpdatePassword();
    }

    /**
     * Create an instance of {@link UpdatePasswordResponse }
     * 
     */
    public UpdatePasswordResponse createUpdatePasswordResponse() {
        return new UpdatePasswordResponse();
    }

    /**
     * Create an instance of {@link UserLimitedDTO }
     * 
     */
    public UserLimitedDTO createUserLimitedDTO() {
        return new UserLimitedDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "editProfile")
    public JAXBElement<EditProfile> createEditProfile(EditProfile value) {
        return new JAXBElement<EditProfile>(_EditProfile_QNAME, EditProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "editProfileResponse")
    public JAXBElement<EditProfileResponse> createEditProfileResponse(EditProfileResponse value) {
        return new JAXBElement<EditProfileResponse>(_EditProfileResponse_QNAME, EditProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfileWithLastName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "editProfileWithLastName")
    public JAXBElement<EditProfileWithLastName> createEditProfileWithLastName(EditProfileWithLastName value) {
        return new JAXBElement<EditProfileWithLastName>(_EditProfileWithLastName_QNAME, EditProfileWithLastName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfileWithLastNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "editProfileWithLastNameResponse")
    public JAXBElement<EditProfileWithLastNameResponse> createEditProfileWithLastNameResponse(EditProfileWithLastNameResponse value) {
        return new JAXBElement<EditProfileWithLastNameResponse>(_EditProfileWithLastNameResponse_QNAME, EditProfileWithLastNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getUser")
    public JAXBElement<GetUser> createGetUser(GetUser value) {
        return new JAXBElement<GetUser>(_GetUser_QNAME, GetUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getUserResponse")
    public JAXBElement<GetUserResponse> createGetUserResponse(GetUserResponse value) {
        return new JAXBElement<GetUserResponse>(_GetUserResponse_QNAME, GetUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getUserRole")
    public JAXBElement<GetUserRole> createGetUserRole(GetUserRole value) {
        return new JAXBElement<GetUserRole>(_GetUserRole_QNAME, GetUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getUserRoleResponse")
    public JAXBElement<GetUserRoleResponse> createGetUserRoleResponse(GetUserRoleResponse value) {
        return new JAXBElement<GetUserRoleResponse>(_GetUserRoleResponse_QNAME, GetUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updatePassword")
    public JAXBElement<UpdatePassword> createUpdatePassword(UpdatePassword value) {
        return new JAXBElement<UpdatePassword>(_UpdatePassword_QNAME, UpdatePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updatePasswordResponse")
    public JAXBElement<UpdatePasswordResponse> createUpdatePasswordResponse(UpdatePasswordResponse value) {
        return new JAXBElement<UpdatePasswordResponse>(_UpdatePasswordResponse_QNAME, UpdatePasswordResponse.class, null, value);
    }

}
