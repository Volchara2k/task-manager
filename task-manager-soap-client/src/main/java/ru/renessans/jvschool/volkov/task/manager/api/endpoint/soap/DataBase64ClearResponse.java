
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataBase64ClearResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dataBase64ClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clearBase64DataFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataBase64ClearResponse", propOrder = {
    "clearBase64DataFlag"
})
public class DataBase64ClearResponse {

    protected boolean clearBase64DataFlag;

    /**
     * Gets the value of the clearBase64DataFlag property.
     * 
     */
    public boolean isClearBase64DataFlag() {
        return clearBase64DataFlag;
    }

    /**
     * Sets the value of the clearBase64DataFlag property.
     * 
     */
    public void setClearBase64DataFlag(boolean value) {
        this.clearBase64DataFlag = value;
    }

}
