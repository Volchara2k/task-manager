
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for lockUserByLoginResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="lockUserByLoginResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lockdownUserFlag" type="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lockUserByLoginResponse", propOrder = {
    "lockdownUserFlag"
})
public class LockUserByLoginResponse {

    protected UserLimitedDTO lockdownUserFlag;

    /**
     * Gets the value of the lockdownUserFlag property.
     * 
     * @return
     *     possible object is
     *     {@link UserLimitedDTO }
     *     
     */
    public UserLimitedDTO getLockdownUserFlag() {
        return lockdownUserFlag;
    }

    /**
     * Sets the value of the lockdownUserFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserLimitedDTO }
     *     
     */
    public void setLockdownUserFlag(UserLimitedDTO value) {
        this.lockdownUserFlag = value;
    }

}
