
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userUnlimitedDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userUnlimitedDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lockdown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userRoleType" type="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}userRoleType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userUnlimitedDTO", propOrder = {
    "lockdown",
    "passwordHash",
    "userRoleType"
})
public class UserUnlimitedDTO
    extends UserLimitedDTO
{

    protected Boolean lockdown;
    protected String passwordHash;
    @XmlSchemaType(name = "string")
    protected UserRoleType userRoleType;

    /**
     * Gets the value of the lockdown property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLockdown() {
        return lockdown;
    }

    /**
     * Sets the value of the lockdown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockdown(Boolean value) {
        this.lockdown = value;
    }

    /**
     * Gets the value of the passwordHash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the value of the passwordHash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordHash(String value) {
        this.passwordHash = value;
    }

    /**
     * Gets the value of the userRoleType property.
     * 
     * @return
     *     possible object is
     *     {@link UserRoleType }
     *     
     */
    public UserRoleType getUserRoleType() {
        return userRoleType;
    }

    /**
     * Sets the value of the userRoleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserRoleType }
     *     
     */
    public void setUserRoleType(UserRoleType value) {
        this.userRoleType = value;
    }

}
