
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unlockUserByLoginResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unlockUserByLoginResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="unLockdownUserFlag" type="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unlockUserByLoginResponse", propOrder = {
    "unLockdownUserFlag"
})
public class UnlockUserByLoginResponse {

    protected UserLimitedDTO unLockdownUserFlag;

    /**
     * Gets the value of the unLockdownUserFlag property.
     * 
     * @return
     *     possible object is
     *     {@link UserLimitedDTO }
     *     
     */
    public UserLimitedDTO getUnLockdownUserFlag() {
        return unLockdownUserFlag;
    }

    /**
     * Sets the value of the unLockdownUserFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserLimitedDTO }
     *     
     */
    public void setUnLockdownUserFlag(UserLimitedDTO value) {
        this.unLockdownUserFlag = value;
    }

}
