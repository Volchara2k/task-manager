
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataYamlClearResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dataYamlClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clearYamlDataFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataYamlClearResponse", propOrder = {
    "clearYamlDataFlag"
})
public class DataYamlClearResponse {

    protected boolean clearYamlDataFlag;

    /**
     * Gets the value of the clearYamlDataFlag property.
     * 
     */
    public boolean isClearYamlDataFlag() {
        return clearYamlDataFlag;
    }

    /**
     * Sets the value of the clearYamlDataFlag property.
     * 
     */
    public void setClearYamlDataFlag(boolean value) {
        this.clearYamlDataFlag = value;
    }

}
