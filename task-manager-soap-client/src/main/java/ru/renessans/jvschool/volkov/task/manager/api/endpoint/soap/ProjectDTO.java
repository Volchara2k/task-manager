
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for projectDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="projectDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}abstractUserOwnerDTO"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDTO")
public class ProjectDTO
    extends AbstractUserOwnerDTO
{


}
