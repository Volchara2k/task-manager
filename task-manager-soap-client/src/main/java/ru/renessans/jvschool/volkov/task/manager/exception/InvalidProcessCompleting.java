package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidProcessCompleting extends AbstractException {

    @NotNull
    private static final String PROCESS_COMPLETING_ILLEGAL = "Нелегальный результат операции!\n";

    public InvalidProcessCompleting() {
        super(PROCESS_COMPLETING_ILLEGAL);
    }

}