
package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePasswordByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePasswordByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="updatedUser" type="{http://soap.endpoint.api.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePasswordByIdResponse", propOrder = {
    "updatedUser"
})
public class UpdatePasswordByIdResponse {

    protected UserLimitedDTO updatedUser;

    /**
     * Gets the value of the updatedUser property.
     * 
     * @return
     *     possible object is
     *     {@link UserLimitedDTO }
     *     
     */
    public UserLimitedDTO getUpdatedUser() {
        return updatedUser;
    }

    /**
     * Sets the value of the updatedUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserLimitedDTO }
     *     
     */
    public void setUpdatedUser(UserLimitedDTO value) {
        this.updatedUser = value;
    }

}
