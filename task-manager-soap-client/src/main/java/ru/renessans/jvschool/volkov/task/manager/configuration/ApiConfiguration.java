package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IApiConfiguration;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IAdminSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IDataInterChangeSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.*;

@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApiConfiguration implements IApiConfiguration {

    @Bean
    @NotNull
    @Override
    public AdminSoapEndpointService adminSoapEndpointService() {
        return new AdminSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationSoapEndpointService authenticationEndpointService() {
        return new AuthenticationSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public DataInterChangeSoapEndpointService dataInterChangeSoapEndpointService() {
        return new DataInterChangeSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public UserSoapEndpointService userSoapEndpointService() {
        return new UserSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public IAdminSoapEndpoint adminSoapEndpoint(
            @NotNull final AdminSoapEndpointService adminSoapEndpointService
    ) {
        return adminSoapEndpointService.getAdminSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationSoapEndpoint authenticationEndpoint(
            @NotNull final AuthenticationSoapEndpointService authenticationSoapEndpointService
    ) {
        return authenticationSoapEndpointService.getAuthenticationSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public IDataInterChangeSoapEndpoint dataInterChangeSoapEndpoint(
            @NotNull final DataInterChangeSoapEndpointService dataInterChangeSoapEndpointService
    ) {
        return dataInterChangeSoapEndpointService.getDataInterChangeSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public ProjectSoapEndpoint projectEndpoint(
            @NotNull final ProjectSoapEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public TaskSoapEndpoint taskEndpoint(
            @NotNull final TaskSoapEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public UserSoapEndpoint userSoapEndpointService(
            @NotNull final UserSoapEndpointService userSoapEndpointService
    ) {
        return userSoapEndpointService.getUserSoapEndpointPort();
    }

}