package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidPortException extends AbstractException {

    @NotNull
    private static final String EMPTY_SESSION = "Параметр \"порт соединения\" отсутствует!\n";

    public InvalidPortException() {
        super(EMPTY_SESSION);
    }

}