package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AbstractEntityDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidProcessCompleting;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidAbstractModelException;
import ru.renessans.jvschool.volkov.task.manager.utility.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@UtilityClass
public final class ViewUtil {

    @NotNull
    public static final String SUCCESSFUL_RESULT = "Операция завершилась успешно!\n";

    @NotNull
    private static final String UNSUCCESSFUL_RESULT = "Операция завершилась неуспешно!\n";

    public void print(@NotNull final String aString) {
        System.out.println(aString);
    }

    @SneakyThrows
    public void print(final boolean aBoolean) {
        if (!aBoolean) {
            print(UNSUCCESSFUL_RESULT);
            throw new InvalidProcessCompleting();
        }
        print(SUCCESSFUL_RESULT);
    }

    @SneakyThrows
    public void print(final int aInt) {
        if (aInt == 0) {
            print(UNSUCCESSFUL_RESULT);
            throw new InvalidProcessCompleting();
        }
        print(SUCCESSFUL_RESULT);
    }

    public void print(@Nullable final Collection<?> aCollection) {
        if (ValidRuleUtil.isNullOrEmpty(aCollection)) {
            print("Список на текущий момент пуст.");
            return;
        }

        @NotNull final AtomicInteger index = new AtomicInteger(1);
        aCollection.forEach(element -> {
            print(index + ". " + element);
            index.getAndIncrement();
        });
        print(SUCCESSFUL_RESULT);
    }

    @SneakyThrows
    public void print(@Nullable final AbstractEntityDTO aEntityDTO) {
        if (Objects.isNull(aEntityDTO)) {
            print(UNSUCCESSFUL_RESULT);
            throw new InvalidAbstractModelException();
        }
        print(aEntityDTO.toString());
    }

}