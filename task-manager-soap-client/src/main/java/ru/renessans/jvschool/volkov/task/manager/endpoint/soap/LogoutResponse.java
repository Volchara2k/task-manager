
package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for logoutResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="logoutResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="logoutFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "logoutResponse", propOrder = {
    "logoutFlag"
})
public class LogoutResponse {

    protected boolean logoutFlag;

    /**
     * Gets the value of the logoutFlag property.
     * 
     */
    public boolean isLogoutFlag() {
        return logoutFlag;
    }

    /**
     * Sets the value of the logoutFlag property.
     * 
     */
    public void setLogoutFlag(boolean value) {
        this.logoutFlag = value;
    }

}
