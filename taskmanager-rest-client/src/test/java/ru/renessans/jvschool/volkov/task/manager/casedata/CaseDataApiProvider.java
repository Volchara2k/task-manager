package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.domain.dto.TimeFrameDTO;

import java.util.UUID;

public final class CaseDataApiProvider {

    @NotNull
    public TaskDTO createTaskDTO() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(UUID.randomUUID().toString());
        taskDTO.setUserId(UUID.randomUUID().toString());
        taskDTO.setProjectId(UUID.randomUUID().toString());
        taskDTO.setTitle("title");
        taskDTO.setDescription("description");
        taskDTO.setStatus(TaskDTO.StatusEnum.NOT_STARTED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        taskDTO.setTimeFrame(timeFrameDTO);
        return taskDTO;
    }

    @NotNull
    public ProjectDTO createProjectDTO() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(UUID.randomUUID().toString());
        projectDTO.setUserId(UUID.randomUUID().toString());
        projectDTO.setTitle("title");
        projectDTO.setDescription("description");
        projectDTO.setStatus(ProjectDTO.StatusEnum.NOT_STARTED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        projectDTO.setTimeFrame(timeFrameDTO);
        return projectDTO;
    }

}