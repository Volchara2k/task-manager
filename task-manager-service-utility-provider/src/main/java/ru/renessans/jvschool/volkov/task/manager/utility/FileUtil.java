package ru.renessans.jvschool.volkov.task.manager.utility;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@UtilityClass
@SuppressWarnings("unused")
public final class FileUtil {

    @NotNull
    @SneakyThrows
    public File create(@Nullable final String pathname) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        delete(pathname);
        @NotNull final File file = new File(pathname);
        Files.createFile(file.toPath());
        return file;
    }

    @SneakyThrows
    public byte[] read(@Nullable final String pathname) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        return Files.readAllBytes(Paths.get(pathname));
    }

    @SneakyThrows
    public boolean delete(@Nullable final String pathname) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) return false;
        if (isNotExists(pathname)) return false;
        @NotNull final Path path = Paths.get(pathname);
        return Files.deleteIfExists(path);
    }

    public boolean isNotExists(@Nullable final File file) {
        if (Objects.isNull(file)) return false;
        return !file.exists() || file.isDirectory();
    }

    public boolean isNotExists(@Nullable final String pathname) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) return false;
        @NotNull final File file = new File(pathname);
        return isNotExists(file);
    }

}