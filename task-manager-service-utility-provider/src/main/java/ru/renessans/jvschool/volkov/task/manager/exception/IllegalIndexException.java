package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class IllegalIndexException extends AbstractException {

    @NotNull
    private static final String INDEX_ILLEGAL =
            "Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    public IllegalIndexException(@NotNull final String message) {
        super(String.format(INDEX_ILLEGAL, message));
    }

}