package ru.renessans.jvschool.volkov.task.manager.utility;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class ConversionUtil {

    @NotNull
    public String asReadableStingFrom(final long from) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if ((from >= 0) && (from < kilobyte)) {
            return from + " B";
        } else if ((from >= kilobyte) && (from < megabyte)) {
            return (from / kilobyte) + " KB";
        } else if ((from >= megabyte) && (from < gigabyte)) {
            return (from / megabyte) + " MB";
        } else if ((from >= gigabyte) && (from < terabyte)) {
            return (from / gigabyte) + " GB";
        } else if (from >= terabyte) {
            return (from / terabyte) + " TB";
        } else {
            return from + " Bytes";
        }
    }

}