package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidParamException extends AbstractException {

    public InvalidParamException(@NotNull final String message) {
        super(String.format("Недопустимый ожидаемый параметр: %s!", message));
    }

}