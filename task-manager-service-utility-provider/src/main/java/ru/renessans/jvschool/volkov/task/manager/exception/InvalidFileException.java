package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidFileException extends AbstractException {

    @NotNull
    private static final String EMPTY_FILE = "Ошибка! Параметр \"файл\" отсутствует!\n";

    public InvalidFileException() {
        super(EMPTY_FILE);
    }

}