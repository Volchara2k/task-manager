package ru.renessans.jvschool.volkov.task.manager.utility;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
@SuppressWarnings("unused")
public final class SystemUtil {

    @NotNull
    private static final String HARDWARE_DATA_PATTERN =
            "Доступные процессоры (ядра): %s; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    @NotNull
    private static final String MAX_MEMORY_VALUE = "без ограничений";

    @NotNull
    public String hardwareDataAsString() {
        return String.format(
                HARDWARE_DATA_PATTERN,
                availableProcessorsAsString(),
                freeMemoryAsString(),
                maxMemoryAsString(),
                totalMemoryAsString(),
                usedMemoryAsString()
        );
    }

    @NotNull
    public String availableProcessorsAsString() {
        return String.valueOf(RuntimeUtil.getAvailableProcessors());
    }

    @NotNull
    public String freeMemoryAsString() {
        final long freeMemory = RuntimeUtil.getFreeMemory();
        return ConversionUtil.asReadableStingFrom(freeMemory);
    }

    @NotNull
    public String maxMemoryAsString() {
        final long maxMemory = RuntimeUtil.getMaxMemory();
        @NotNull final String maxMemoryValue = ConversionUtil.asReadableStingFrom(maxMemory);
        return (maxMemory == Long.MAX_VALUE ? MAX_MEMORY_VALUE : maxMemoryValue);
    }

    @NotNull
    public String totalMemoryAsString() {
        final long totalMemory = RuntimeUtil.getTotalMemory();
        return ConversionUtil.asReadableStingFrom(totalMemory);
    }

    @NotNull
    public String usedMemoryAsString() {
        final long freeMemory = RuntimeUtil.getFreeMemory();
        final long totalMemory = RuntimeUtil.getTotalMemory();
        final long usedMemory = totalMemory - freeMemory;
        return ConversionUtil.asReadableStingFrom(usedMemory);
    }

}