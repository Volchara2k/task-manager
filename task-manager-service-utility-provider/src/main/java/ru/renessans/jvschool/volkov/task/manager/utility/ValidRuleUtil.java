package ru.renessans.jvschool.volkov.task.manager.utility;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;

import java.util.Collection;

@UtilityClass
@SuppressWarnings("unused")
public class ValidRuleUtil {

    @Contract("null -> false")
    public boolean isNotNullOrEmpty(@Nullable final String aString) {
        return StringUtils.hasText(aString);
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final String aString) {
        return !StringUtils.hasText(aString);
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final String... aStrings) {
        return aStrings == null || aStrings.length < 1;
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final Integer aInteger) {
        return aInteger == null || aInteger < 0;
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final Long aLong) {
        return aLong == null || aLong <= 0L;
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final Collection<?> aCollection) {
        if (aCollection == null || aCollection.isEmpty()) return true;
        return aCollection.stream().allMatch(ValidRuleUtil::isNullOrEmpty);
    }

    private boolean isNullOrEmpty(@Nullable final Object aObject) {
        return aObject == null;
    }

}