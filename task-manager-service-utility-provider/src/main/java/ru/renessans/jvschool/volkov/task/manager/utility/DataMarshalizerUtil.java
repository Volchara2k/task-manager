package ru.renessans.jvschool.volkov.task.manager.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.InvalidParamException;

import java.io.File;
import java.util.Objects;

@UtilityClass
@SuppressWarnings("unused")
public class DataMarshalizerUtil {

    @SneakyThrows
    public <T> T writeToJson(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidParamException("t");
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = FileUtil.create(pathname);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromJson(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        if (Objects.isNull(tClass)) throw new InvalidParamException("tClass");
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = new File(pathname);
        return objectMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToXml(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidParamException("t");
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File file = FileUtil.create(pathname);
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromXml(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        if (Objects.isNull(tClass)) throw new InvalidParamException("tClass");
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File file = new File(pathname);
        return xmlMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToYaml(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidParamException("t");
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);
        @NotNull final File file = FileUtil.create(pathname);
        yamlMapper.writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromYaml(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidParamException("pathname");
        if (Objects.isNull(tClass)) throw new InvalidParamException("tClass");
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);
        @NotNull final File file = new File(pathname);
        return yamlMapper.readValue(file, tClass);
    }

}